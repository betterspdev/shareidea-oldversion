import * as React from 'react';
import styles from './TableWp.module.scss';
import { ITableWpProps , ICustomState} from './ITableWpProps';
import { escape, isEmpty } from '@microsoft/sp-lodash-subset';
import { SPHttpClient, SPHttpClientResponse, HttpClient, HttpClientResponse } from '@microsoft/sp-http';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap' ;
import 'jquery';
import 'popper.js';
import * as $ from 'jquery'; 
import * as jQuery from 'jquery'; 

import pnp, { Web, PermissionKind  } from 'sp-pnp-js';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import {faTimesCircle ,faTimes, faCheckCircle , faStar,faUsersCog , faPlusCircle ,faPlus,faUserMinus,faMinus,faMinusCircle,faSearch , faCalendarAlt , faInfoCircle} from '@fortawesome/free-solid-svg-icons';
import { MDBDataTable ,  MDBTable, MDBTableBody, MDBTableHead, MDBInput   } from 'mdbreact'; //Data Table
import { Log } from '@microsoft/sp-core-library';
import * as ReactDOM from 'react-dom';
import { CurrentUser } from 'sp-pnp-js/lib/sharepoint/siteusers';
import "../../../Assets/custom.css";

let typeModal = Object.freeze({
  'sm' : {dialog : "bd-example-modal-sm" , document : "modal-sm"},
  'lg' :{dialog : "bd-example-modal-lg" , document : "modal-lg"},
  'xl' :{dialog : "bd-example-modal-xl" , document : "modal-xl"},
  'center' : {dialog : "" , document : "modal-dialog-centered"},
});
let opacityNoti = 0 ;
let webPnP:Web ;
let siteUser:any = [] ;
let selectedUser = [] ;
let btnTask : any = [] ;
let  newTag : any = [] ;

export default class TableWp extends React.Component<ITableWpProps, ICustomState> {
  private pageName: string = "Collaboration";
  private baseUrl: string = 'https://betteritcorp.sharepoint.com/sites/BETTERTEST/RK';
  private webPnP = new Web(this.baseUrl);

  constructor(props:ITableWpProps , state : ICustomState){
    super(props);
    this.state = {
      notifyModal : 'center',
      alertNotify : 'success',
      startDate: new Date(),
     
      isAdmin : [] ,
      CurrentUserName :'',
      CurrentUserEmail :'',
      CurrentUserID : '',
      jsonData : [] ,
      requestList : [] ,
      showallColumn : [] ,
      allColumn : [] ,
      myView : [] 

     
    };
    this.getCurrentUser();
    this.getListView();
    
  }
  public render(): React.ReactElement<ITableWpProps> {
    let urlPath = this.props.siteUrl == undefined ? this.props.absoluteUrl : this.props.siteUrl ;
    webPnP = new Web(urlPath); 
    library.add(faCalendarAlt,faTimesCircle ,faTimes , faCheckCircle , faUsersCog,faStar , faPlus ,faUserMinus,faMinus,faMinusCircle,faSearch ,faInfoCircle,faPlusCircle);
    return (
      <div className={ styles.tableWp }>
           <div className={styles["form-dropped"]} style={{border:'none'} }>
              <div className={styles.labelForm} >
                  <label >{this.props.description == undefined ? '' : this.props.description}</label>
                </div> 
                <button type="button" className="btn btn-light" onClick={ev=>this.addTopic()}><FontAwesomeIcon icon="plus-circle"  color="green" /> Topic</button>
                {this.DatatablePage()}
              
        
              </div>

               {/* ******************************** Modal *****************************************/}
         <div className={"modal fade "+typeModal[this.state.notifyModal].dialog} id="Modal"  role="dialog" aria-labelledby="Modal" aria-hidden="true">
            <div className={"modal-dialog "+typeModal[this.state.notifyModal].document} role="document">
              <div className="modal-content" id="Modal_content">
            
              </div>
            </div>
          </div>

          <div className={"modal fade "} id="loading"  role="dialog" aria-labelledby="loading" aria-hidden="true">
            <div className={"modal-dialog center"} role="document">
              <div className="modal-content" >
                <div className="modal-body">
                  <strong style={{marginRight:'10px'}}><FontAwesomeIcon icon="spinner" size="lg" color="blue" pulse/></strong >
                  Please waiting for process
                </div>
               
              </div>
            </div>
          </div>

      </div>
    );
  }
  private async getListView(){
    let urlPath = this.props.siteUrl == undefined ? this.props.absoluteUrl : this.props.siteUrl ;
    webPnP = new Web(urlPath); 
      if(!isEmpty(this.props.topicList) ){
        // let isAdmin = await webPnP.lists.getByTitle(this.props.topicList).currentUserHasPermissions(PermissionKind.ManageLists).then(res=>{
        //   return res ;
        // });
      
        let requestList = await webPnP.lists.getByTitle(this.props.topicList).items.orderBy("Created",false).get();
        //let jsonView  = await webPnP.lists.getByTitle(this.props.requestList).fields.filter(`Hidden eq false  and ReadOnlyField eq false and Group ne '_Hidden'`).get();
        let allColumn = await webPnP.lists.getByTitle(this.props.topicList).fields.filter(`Hidden eq false  and ReadOnlyField eq false and Group ne '_Hidden'`).get();
        let showallColumn =  await webPnP.lists.getByTitle(this.props.topicList).fields.filter(`Hidden eq false  and Group ne '_Hidden'`).get();
        let myView = await webPnP.lists.getByTitle(this.props.topicList).defaultView.fields.filter(`Hidden eq false and ReadOnlyField eq false and Group ne '_Hidden'`).get();
        if( !isEmpty(requestList)){
          this.setState({
          
            requestList : requestList,
            //jsonView : jsonView ,
            allColumn : allColumn ,
            myView : myView,
            showallColumn : showallColumn
          });
      }
    }
    
  }

  private addTopic(){
    newTag = [] ;
    let content = [
      <div className={"modal-header "+styles.raleWay}>
      <h5 className="modal-title"> Create New Topic </h5>
      <button type="button" className="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>,
    <div className={"modal-body "+styles.raleWay}>
        <div className="row">

          <label className="col-sm-2"><b>Topic name</b>   </label> 
          <label className="col-sm-10"><input type="text" id="topicName" className="form-control"/></label>
        </div>
        <div className="row">
          <label className="col-sm-2"><b>End Time</b>   </label> 
          <div className="col"><DateInput /></div>
        </div>
        <div className="row">
          <label className="col-sm-2"><b>Description</b>   </label> 
          <label className="col-sm-10"> <textarea className={"form-control "} rows={3} id="Description" ></textarea></label>
        </div>
        <br/>
        <div className="row">
          <label className="col-sm-2"><b>Topic Owner</b>   </label> 
          <label className="col-sm-10"><input type="text" id="topicOwner" className="form-control" value={this.state.CurrentUserName} readOnly/> </label>
         
        </div>
        <div className="row">
          <label className="col-sm-2"><b>HashTag</b>   </label> 
          <label className="col-sm-10"><input type="text" id="topicTag" className="form-control" onKeyDown={ev=>{this.addTag(ev);}}/> </label>
        </div>
        <div className="row">
        <label className="col-sm-2"><b></b>   </label> 
          <label className="col-sm-10" id="tagShow">  </label> 
         
        </div>
        
        <br/>
       
      

    
    </div>,
    <div className="modal-footer">
        <button type="button" data-dismiss="modal" onClick={ev=>{this.createTopic();}} className="btn btn-primary">Submit</button>
      </div>
    

    ];
    this.setState({notifyModal : "lg"});
    ReactDOM.render(content,document.getElementById('Modal_content'));
    $('#Modal').modal("show");
  
  }
  private async createTopic(){
    let topicname:any = $('#topicName').val();
    let Description:any = $('#Description').val();
    let selectDateInput:any = $('#selectDateInput').val();

    let property = {
      Title : `${topicname}` , 
      Description : `${Description}` ,
      EndTime : new Date(selectDateInput) ,
      Hashtag: { results : newTag.length > 0 ?  newTag : [] }
      
    };
    await webPnP.lists.getByTitle(this.props.topicList).items.add(property).then(async(res)=>{
      console.log("Yess");
      this.getListView();
     
      await this.checkListExists();
      await this.checkLogExist("_Log");
      await this.checkLogExist("_Recurrent_Log");
      await this.updateComment(topicname);
      
    },err=>{
      console.log(err);
    });
  }

  public checkLogExist = async(id): Promise<boolean> =>{
    this.webPnP = new Web(this.baseUrl);
    return new Promise<boolean>(async (res, rej)=>{
      this.webPnP.lists.getByTitle(this.pageName+id).get().then((listExists)=>{
        res(true);
      }).catch(async err =>{
        let listExist = await (await this.webPnP.lists.ensure(this.pageName+id)).list;
        await listExist.fields.addText('Log_Type', 255, { Required: false, Description: '' });
        await listExist.fields.addMultilineText('Log_Detail', 6, false, false, false, false, { Required: false, Description: '' });
        let allItemsView = await listExist.views.getByTitle('All Items');
        await allItemsView.fields.add('Log_Type');
        await allItemsView.fields.add('Log_Detail');
      });
    });
  }

  public checkListExists = async (): Promise<boolean> => {
    this.webPnP = new Web(this.baseUrl);
    return new Promise<boolean>(async (res, rej) => {
        this.webPnP.lists.getByTitle(this.pageName).get().then((listExists) => {
            res(true);
        }).catch(async err => {
            let listExists = await (await  this.webPnP.lists.ensure(this.pageName)).list;
            await listExists.fields.addMultilineText('Comments', 6, false, false, false, false, { Required: false, Description: '' });
            //await listExists.fields.addMultilineText('Likes', 6, false, false, false, false, { Required: false, Description: '' });
            await listExists.fields.addMultilineText('Description', 6, false, false, false, false, { Required: false, Description: '' });
            await listExists.fields.addDateTime('StartDate',1);
            await listExists.fields.addDateTime('EndDate',1);
            await listExists.fields.addText('FilesFolder', 255, { Required: false, Description: '' });
            let allItemsView = await listExists.views.getByTitle('All Items');
            await allItemsView.fields.add('IsArchive');
            await allItemsView.fields.add('Comments');
            //await allItemsView.fields.add('Likes');
            await allItemsView.fields.add('Description');
            await allItemsView.fields.add('StartDate');
            await allItemsView.fields.add('EndDate');
            await allItemsView.fields.add('FilesFolder');
            res(true);
        });        
    });
  }

  public updateComment = async (title) => {
    this.webPnP = new Web(this.baseUrl);
    let jsonComments = [];
    await this.webPnP.lists.getByTitle(this.pageName).items.add({
      Title: title
    }).then(async (res)=>{
      await this.updateFilePath(res.data.ID);
    });
  }

  public updateFilePath = async (ID) => {
    await this.createFolder(`Shared Documents/Collaboration/Crisis_${ID}_Documents`);
    await this.webPnP.lists.getByTitle(this.pageName).items.getById(ID).update({
      FilesFolder: `Shared Documents/Collaboration/Crisis_${ID}_Documents`
    });
  }

  public createFolder = async (folderPath) => {
    this.webPnP = new Web(this.baseUrl);
    return await this.webPnP.folders.add(folderPath);
  }
 
  private addTag(e){
    let el = e.target.value ;
    if(e.key === 'Enter'){
      newTag.push(el);
      let tag = newTag.map(t=>{
          return (<span style={{fontSize:"14px",marginRight:"5px"}} className="badge badge-info">{t} <FontAwesomeIcon onClick={ev=>{delTag(t);}} style={{marginLeft:"5px",cursor:"pointer"}} icon="times"  color="#a72f22" /> </span>) ;
      });
      ReactDOM.render(tag,document.getElementById("tagShow"));
    }

   function delTag(t){
    let tmp = newTag ; 
    newTag = tmp.filter(tag=>{return tag !== t}) ;
    let tag = newTag.map(t=>{
      return (<span style={{fontSize:"14px",marginRight:"5px"}} className="badge badge-info">{t} <FontAwesomeIcon onClick={ev=>{delTag(t);}} style={{marginLeft:"5px",cursor:"pointer"}} icon="times"  color="#a72f22" /> </span>) ;
    });
    ReactDOM.render(tag,document.getElementById("tagShow"));
   }
  }
  private async getCurrentUser():Promise<any>{
    let urlPath = this.props.siteUrl == undefined ? this.props.absoluteUrl : this.props.siteUrl ;
    webPnP = new Web(urlPath); 
    //let test = await webPnP.currentUser.get() ; 
    var user = await this.props.spHttpClient.get(`${urlPath}/_api/web/currentuser`,SPHttpClient.configurations.v1,{
      headers: {
      'Accept': 'application/json;odata=nometadata',
      'odata-version': ''
      }
    }).then(response=>{
      return response.json() ; 
    });
    siteUser = await this.props.spHttpClient.get(`${urlPath}/_api/web/siteusers`,SPHttpClient.configurations.v1,{
      headers: {
      'Accept': 'application/json;odata=nometadata',
      'odata-version': ''
      }
    }).then(response=>{
      return response.json() ; 
    });
     this.setState({
      CurrentUserName :  user.Title ,
      CurrentUserEmail : user.UserPrincipalName ,
      CurrentUserID : user.Id
     });

     
     // CurrentUserDepartment :  user2.UserProfileProperties[12].Value // Department 11 , Job Tiltle 12 
     
     
  }
  
  DatatablePage = () => {
    if(!isEmpty(this.state.requestList)){
       let column:any , rows:any;

       let tmpCol = [] ; 
       let tmpData = [] , tmpmy = [] ;
       let test = '';
       let ntmpData = []  ;
       tmpmy.push({ Title : "info" , TypeAsString : "info" , label : "#" });
       tmpData = this.state.requestList;
       tmpCol = this.state.showallColumn ;//this.state.requestList.Items;
       let gg = this.state.myView.Items.filter(col=>{  
          ( tmpCol.filter(tmp=>{
            if(col == 'LinkTitle'){
              col = 'Title';
            }
            if(tmp.StaticName == col) {
              tmpmy.push(tmp);
              return tmp;
            }
              
           })
         );
       });
       tmpmy.push({ Title : "join" , TypeAsString : "join" , label : "" });
       //tmpmy.push({ Title : "info" , TypeAsString : "info" , label : "#" });
       tmpCol = tmpmy ;
       let data = tmpData.map((data,count)=>{
         let nData:any = [] ;
         test = '';
         let dataCol = tmpCol.map((col,index)=>{
           
           if(col.Title == 'LinkTitle'){
             col.Title = 'Title';
           }
          
           //let item = data[col.Title] ;
           if(data[col.Title]  instanceof Array ){
             let item = data[col.Title] ;
             console.log('array',data[col.Title] );
             let arr = item.map(i=>{
               return(<span className="badge badge-info">{i}</span>);
             });
           nData[`${col.Title}`] = arr; 
           }
           
           
           else{
            if(col.TypeAsString == 'User'){
              // col.Title = 'AuthorId';
              if(col.Title == "Created By"){
                let user = siteUser.value.filter(u=>{ return u.Id == data['AuthorId']; }) ;
                nData[`Author`] = user[0].Title ; 
              }
              else{
                let user = siteUser.value.filter(u=>{ return u.Id == data[`${col.Title}Id`]; }) ;
                if(!isEmpty(user)){
                  nData[`${col.Title}`] = user[0].Title ; 
                }
                else{
                  nData[`${col.Title}`] = '' ; 
                }
               
              }
             
             //let gg = this._getDateformate(item);
            }
            else if(col.Title == "join"){
              nData[`${col.Title}`] = <button type="button" onClick={ev=>{ this.JoinPage(data);}} className={styles.noBorder + " btn btn-info" }><FontAwesomeIcon icon="info-circle"/> Join Topic</button> ; 
            }
            else if(col.Title == "info"){
              if(count <= 1 ){
                nData[`${col.Title}`] = [<span className="badge badge-danger blink">New</span> , <div style={{fontSize:"10px"}}>{this.getStar(data)}</div>]; 
              }
              else {
                nData[`${col.Title}`] = [ <div style={{fontSize:"10px"}}>{this.getStar(data)}</div>]; 
              }

             
            }
         
             else{
              let item = data[col.Title] ;
              nData[`${col.Title}`] = col.TypeAsString == 'DateTime' ?  this._getDateformate(item) : item   ; 
             }
             
           }
           
           let colName = col.Title ;
           //return { colName : col.TypeAsString == 'DateTime' ?  this._getDateformate(item) : item } ;
         });
         nData['clickEvent']  = () =>this.handleClickTr(data) ;
         //column.push(test);
         ntmpData.push(nData);
         return dataCol;
       });

      rows = ntmpData;

      column = tmpCol.map(col=>{
        if(col.Title == 'LinkTitle'){
          col.Title = 'Title' ; 
        }
        else if(col.Title == 'Created By'){
          col.Title = 'Author';
        }

          return{ label : col.Title == "info" ? "#" : col.Title=="join" ? "" : col.Title  , field : col.Title ,sort: "asc" , width : col.Title == 'Title' ? 300  : 150} ;

      });
      // console.log(column);
      // console.log(rows);
      let dataTable = { columns : column , rows : rows} ; 
      return (
       <MDBDataTable 
       noBottomColumns
       sorting
       paging={this.props.paging == undefined ? false : this.props.paging}
       searching={this.props.searching == undefined ? false : this.props.searching}
       hover
         data={dataTable}
       />
       );
     }



 }

 private handleClickTr(item){
    let imgPath = require<string>("../../../image/topic.png") ;
    let user = siteUser.value.filter(u=>{ return u.Id == item.AuthorId; }) ;
    let isVoted ; 
    if(!isEmpty(item.RatedByStringId) && !isEmpty(user[0])){
      isVoted = item.RatedByStringId.filter(id=>{ return id == this.state.CurrentUserID});
    }
    
    console.log(isVoted);
    
    let hashtag =  item.Hashtag.map(i=>{
      return(<span style={{marginLeft:'5px',fontSize:"14px"}} className="badge badge-info">#{i}</span>);
    });
    let content = [
      <div className={"modal-header "+styles.raleWay}>
      <h5 className="modal-title"> <img src={this.props.absoluteUrl+"/PublishingImages/test/topic.png"} style={{marginRight: '12px' ,width: '40px' ,overflow: 'hidden'}}/> Topic : {item.Title}</h5>
      <button type="button" className="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>,
    <div className={"modal-body "+styles.raleWay}>
      <div style={{textAlign:"center",marginBottom:'50px'}}>
        <img src={this.props.absoluteUrl+"/PublishingImages/"+item.image} style={{marginRight: '12px' ,width: '60px',borderRadius: '50%' ,overflow: 'hidden'}}/>
      </div>
      <div className="row">

          <label className="col-sm-2"><b>Topic name</b>   </label> 
          <label className="col-sm-5">{item.Title}</label>
          <label className="col-sm-2"><b>End Time</b>   </label> 
          <label className="col-sm-3">{this._getDateformate(item.EndTime)}</label>
        </div>
        <div className="row">
          <label className="col-sm-2"><b>Description</b>   </label> 
          <label className="col-sm-10">{item.Description}</label>
        </div>
        <br/>
        <div className="row">
          <label className="col-sm-2"><b>Topic Owner</b>   </label> 
          <label className="col-sm-5">{user[0].Title}</label>
         
        </div>
        <div className="row">
          <label className="col-sm-2"><b>HashTag</b>   </label> 
         
         
        </div>
        <div className="row">
          <label className="col-sm-2">   </label> 
          <label className="col-sm-10">{hashtag}</label>
         
        </div>

        <div className="row">
          <label className="col-sm-2"> <b>Rating</b>   </label> 
          <label className="col-sm-10">{ isEmpty(isVoted) == true ? [
          <FontAwesomeIcon style={{cursor:"pointer",marginRight:"2px"}} data-dismiss="modal" onClick={ev=>{this.VoteTopic(item,1);}} icon="star" id="Star" />,
          <FontAwesomeIcon style={{cursor:"pointer",marginRight:"2px"}} data-dismiss="modal" onClick={ev=>{this.VoteTopic(item,2);}} id="2Stars" icon="star"  />,
          <FontAwesomeIcon style={{cursor:"pointer",marginRight:"2px"}} data-dismiss="modal" onClick={ev=>{this.VoteTopic(item,3);}} icon="star" id="3Stars" />,
          <FontAwesomeIcon  style={{cursor:"pointer",marginRight:"2px"}} data-dismiss="modal" onClick={ev=>{this.VoteTopic(item,4);}} icon="star" id="4Stars" />,
          <FontAwesomeIcon  style={{cursor:"pointer",marginRight:"2px"}} data-dismiss="modal" onClick={ev=>{this.VoteTopic(item,5);}} icon="star" id="5Stars" /> , 
          <p style={{fontWeight:400,color:"#636363"}} >(Rating this Topic)</p>]
          : ''}   {[this.showrating(item) + `- ${item.AverageRating.toFixed(2)} (${item.RatingCount})`, this.getStar(item) ] }</label>
         
        </div>
    </div>,
      <div className="modal-footer">
      <button type="button" onClick={ev=>{ this.JoinPage(item);}} className={styles.noBorder + " btn btn-info btn  btn-block" }><FontAwesomeIcon icon="info-circle"/> Join Topic</button>
    </div>
 
    

    ];
    this.setState({
      notifyModal : "lg"
    });
    ReactDOM.render(content,document.getElementById('Modal_content'));
    $('#Modal').modal("show");
  
 

 }
 
 private async VoteTopic(item,rating){
  let ratingaverage ;//= parseFloat(item.AverageRating) ;
  let ratingcount ;//= item.RatingCount ; 
  let tmp  =  [] ;
  let rateBy = item.RatedByStringId ;
  
  if(isEmpty(rateBy)){
    tmp.push(this.state.CurrentUserID.toString());
    ratingaverage = 0 ;
    ratingcount = 0 ;
  } 
  else{
    tmp = rateBy ; 
    tmp.push(this.state.CurrentUserID.toString());
    ratingaverage = parseFloat(item.AverageRating) ;
    ratingcount = item.RatingCount ; 
  }
  
  let newRatedby = tmp ; 
  var ratingaverageNew = (ratingaverage*ratingcount+rating)/(newRatedby.length) ; 

   let property = {
    AverageRating : ratingaverageNew,
    RatingCount : newRatedby.length,
    RatedByStringId : { results : newRatedby }
   }

   await webPnP.lists.getByTitle(this.props.topicList).items.getById(item.Id).update(property).then(res=>{
    console.log("Voted",res.data);
    this.getListView();
    
   }),(err=>{
    console.log("Error");
   });
 }

 private getStar(myItem){
  var AverageRating = parseFloat(myItem.AverageRating) ; 
    if(isEmpty(AverageRating)){
      
      var content1 = AverageRating == 0 ? "" :
      AverageRating >= 1 && AverageRating < 2 ? <FontAwesomeIcon icon="star" color="gold" /> : 
      AverageRating >= 2 && AverageRating < 3 ? [<FontAwesomeIcon icon="star" color="gold" />,<FontAwesomeIcon icon="star" color="gold" />] :
      AverageRating >= 3 && AverageRating < 4 ? [<FontAwesomeIcon icon="star" color="gold" />,<FontAwesomeIcon icon="star" color="gold" />,<FontAwesomeIcon icon="star" color="gold" />] : 
      AverageRating >= 4 && AverageRating < 5 ? [<FontAwesomeIcon icon="star" color="gold" />,<FontAwesomeIcon icon="star" color="gold" />,<FontAwesomeIcon icon="star" color="gold" />,<FontAwesomeIcon icon="star" color="gold" />] : 
      AverageRating  == 5 ? [<FontAwesomeIcon icon="star" color="gold" />,<FontAwesomeIcon icon="star" color="gold" />,<FontAwesomeIcon icon="star" color="gold" />,<FontAwesomeIcon icon="star" color="gold" />,<FontAwesomeIcon icon="star" color="gold" />] : "" ;
      return content1 ;
   }
 
 }
 private showrating(myItem):string{
  var AverageRating = parseFloat(myItem.AverageRating) ; 
  var Count = myItem.RatingCount ; 
  var content = AverageRating == 0 ? "No Rating" : AverageRating >= 1 && AverageRating < 2 ? "Poor" : AverageRating >= 2 && AverageRating < 3 ? "Not Bad" :
  AverageRating >= 3 && AverageRating < 4 ? "Average" : AverageRating >= 4 && AverageRating < 5 ? "Good" : "Excellent" ;
  return content.toString() ;

  
 }


 private JoinPage(item){
  var enc = window.btoa(item.Title);

  window.location.href = this.props.joinPage+`#${enc}#${item.Id}` ;
 }

 public monthNames = ["January", "February", "March", "April", "May", "June",
 "July", "August", "September", "October", "November", "December"];
 private _getDateformate(dateString:string):string{
  
   var date:Date = new Date(dateString);
   
     var hours = date.getHours();
     var minutes = date.getMinutes();
     var ampm = hours >= 12 ? 'PM' : 'AM';
     hours = hours % 12;
     hours = hours ? hours : 12; // the hour '0' should be '12'
     var min = minutes < 10 ? '0'+minutes : minutes;
     var strTime = hours + ':' + min + ' ' + ampm;
     return this.monthNames[date.getMonth()]+" "+ date.getDate() + ", "+date.getFullYear() ; //date.getDate()+"/"+ (date.getMonth()+1) + "/"+date.getFullYear() ;//+" "+strTime;
   
  
  
 }
}
class DateInput extends React.Component {
  state = {
    startDate: new Date()
  };
 
  handleChange = date => {
    this.setState({
      startDate: date
    });
  }

  
 
  render() {
    const DatePickerInput = ({ value, onClick }) => (
      <input
          name={"selectDate"}
          id={"selectDate"+"Input"}
          value={value}
          onChange={e => onClick(e.target.value,"selectDate")}
          onClick={e => onClick(value,"selectDate")}
      />
   
  ); 
    return (
      // <DatePicker
      //   selected={this.state.startDate}
      //   onChange={this.handleChange}
      // />
      <div></div>
      // <DatePicker  
      // selected={this.state.startDate}  
      // onChange={date => this.handleChange(date)}
      // customInput={ <DatePickerInput  value={this.state.startDate} onClick={date => this.handleChange(date)}  />}
      // />
    );
  }

}
