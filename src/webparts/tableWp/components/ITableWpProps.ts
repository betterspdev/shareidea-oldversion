export interface ITableWpProps {
  description: string;
  absoluteUrl : string ;
  siteUrl : string ;
  topicList : string ;
  spHttpClient : any ;
  joinPage : string ;
  searching : boolean ;
  paging : boolean ; 


}

export interface ICustomState{
  notifyModal : any ;
  startDate: any ;
  alertNotify : any;
  isAdmin : any ;
  CurrentUserName : string ;
  CurrentUserEmail : string ;
  CurrentUserID : string ; 

  requestList : any ; 
  jsonData : any ;
  myView : any ;
  showallColumn : any ;
  allColumn : any ;

 
}
