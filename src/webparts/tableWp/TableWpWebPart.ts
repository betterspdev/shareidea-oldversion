import * as React from 'react';
import * as ReactDom from 'react-dom';
import { Version } from '@microsoft/sp-core-library';
import {
  IPropertyPaneConfiguration,
  PropertyPaneTextField,
  PropertyPaneToggle
} from '@microsoft/sp-property-pane';
import { BaseClientSideWebPart } from '@microsoft/sp-webpart-base';

import * as strings from 'TableWpWebPartStrings';
import TableWp from './components/TableWp';
import { ITableWpProps } from './components/ITableWpProps';

export interface ITableWpWebPartProps {
  description: string;
}

export default class TableWpWebPart extends BaseClientSideWebPart <ITableWpProps> {

  public render(): void {
    const element: React.ReactElement<ITableWpProps> = React.createElement(
      TableWp,
      {
        description: this.properties.description,
        siteUrl : this.properties.siteUrl , 
        topicList : this.properties.topicList,
        absoluteUrl : this.context.pageContext.web.absoluteUrl ,
        spHttpClient : this.context.spHttpClient,
        joinPage : this.properties.joinPage ,
        searching : this.properties.searching ,
        paging : this.properties.paging , 
      }
    );

    ReactDom.render(element, this.domElement);
  }

  protected onDispose(): void {
    ReactDom.unmountComponentAtNode(this.domElement);
  }

  protected get dataVersion(): Version {
    return Version.parse('1.0');
  }

  protected getPropertyPaneConfiguration(): IPropertyPaneConfiguration {
    return {
      pages: [
        {
          header: {
            description: strings.PropertyPaneDescription
          },
          groups: [
            {
              groupName: strings.BasicGroupName,
              groupFields: [
                PropertyPaneTextField('description', {
                  label: strings.DescriptionFieldLabel
                }),
                PropertyPaneTextField('topicList', {
                  label: "Topic List"
                }),
                PropertyPaneTextField('joinPage', {
                  label: "Join Topic Url"
                }),
                PropertyPaneTextField('siteUrl', {
                  label: "Site Url"
                }),
                PropertyPaneToggle('searching', {
                  label: "Searching on Table",
                  onText : "On",
                  offText: "Off"
                }),
                PropertyPaneToggle('paging', {
                  label: "Pagination on Table",
                  onText : "On",
                  offText: "Off"
                }),
              ]
            }
          ]
        }
      ]
    };
  }
}
