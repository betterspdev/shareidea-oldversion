declare interface ICollaborationWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
}

declare module 'CollaborationWebPartStrings' {
  const strings: ICollaborationWebPartStrings;
  export = strings;
}
