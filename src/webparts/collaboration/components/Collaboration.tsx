import * as React from 'react';
import styles from './Collaboration.module.scss';
import { ICollaborationProps } from './ICollaborationProps';
import { IState } from './IState';
import { escape, has, isEmpty, random } from '@microsoft/sp-lodash-subset';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faRedo, faReply, faSearch, faShareAlt, faThumbtack, faTimes, faPaperPlane, faUsers, faFlag, faCalendarAlt, faCaretRight, faCaretUp, faPaperclip, faCaretDown, faCaretLeft, faBars, faPlus, faAngleLeft, faAngleRight, faArrowUp, faArrowDown } from '@fortawesome/free-solid-svg-icons';
import { library, text } from '@fortawesome/fontawesome-svg-core';
import { SPComponentLoader } from '@microsoft/sp-loader';
import { SPHttpClient, SPHttpClientResponse, HttpClient, HttpClientResponse } from '@microsoft/sp-http';


import 'jquery';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'popper.js';

import { sp } from '@pnp/sp';
import pnp, { Web, List, ConsoleListener } from 'sp-pnp-js';
import * as moment from 'moment';
import * as _ from "lodash";
import { Environment } from '@microsoft/sp-core-library';
import { Group } from 'sp-pnp-js/lib/graph/groups';

import { MDBDataTable , MDBDataTableV5 } from 'mdbreact'; //Data Table
import { concatStyleSets, selectProperties, ThemeSettingName } from 'office-ui-fabric-react';
require("../css/custom.css");
require("../css/custom_style.css");

let monthNames = ["January", "February", "March", "April", "May", "June",
"July", "August", "September", "October", "November", "December"];
let datNames = ["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"];

let ActionButton = [
  {Title:"New Post",Color:"#6c42f5",Action:["Basic action","Intermediate action","Advance action"]},
];

let todayDate = new Date();
let changeInnerHTML = '';
let firstCall = 0;
let captureMouseDown = 0;
let cickedBtn = null;

export default class Collaboration extends React.Component<ICollaborationProps, IState> {
  private pageName: string = "Collaboration";
  private siteUsers: any[] = [];
  private pageurl: string = '';
  private baseUrl: string = 'https://betteritcorp.sharepoint.com/sites/BETTERTEST/RK';
  private webPnP = new Web(this.baseUrl);

  constructor(props:ICollaborationProps, state: IState){
    super(props);

    SPComponentLoader.loadCss("https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css");

    this.state={
      collabID: -1,
      settingSeemore: '',
      createdDate: '',
      modifiedDate: '',

      onSearching: false,
      searchTxt: '',
      searchSelectScroll: false,
      glowingPost: false,

      currentState: '',
      allComments: [],
      enablePing: false,
      commentText: false,
      currentTab: '',
      currentUserInfo: null,
      scroll: 0,
      scrollH: 0,
      scrollCapture: false,
      startCapture: 0,
      firstRender: false,
      changeScroll: false,
      parentPost: '',
      toggleReply: false,
      suggestBlockPosition: null,
      suggestBlockID: '',
      suggestSearch: '',
      filterSuggest: [],
      suggestSelect: [],

      userActionPermission: '',
      userGroup: [],

      shareWith: [],
      currentPin: 0,

      toggleShareID: '',
      shareSelect: [],
      shareSelectID: -1,

      allTasks: [],

      attachmentSelect: [],
      fileFolder: '',

      //Flag
      toggleFlagID: '',
      flagKeySelected: '',

      //Widget
      currentWidget: '',

      //Assign Task widget
      selectStatus :["Complete","Processing","Delay","Waiting"],
      dataTask: [],
      
      selected_lane: 'IT',
      
      newPostComment: [],
      alertStatus: '',
      toggleMobileNav: false,
      pageMessage: 0,
      pageFile: 0,
      newCommentChange: false,

      currentTablePage: 0,
      searchTable: '',
      sortTable: '',
      tooltipMobile: '',
      onLoading: '',
      onSendRequest: [],
      onReplyLoading: [],
      displayAlert: 'none',
      newTextAlert: '',

      parentTopic: '',
      parentItem: null,
      parentUser: null
    };

    this.checkAndCreateList();
  }

  public render(): React.ReactElement<ICollaborationProps> {
    library.add(faSearch, faRedo, faTimes, faReply, faThumbtack, 
      faShareAlt, faPaperPlane, faUsers, faFlag, faCalendarAlt, 
      faCaretRight, faCaretUp, faPaperclip, faCaretDown, 
      faCaretLeft, faBars, faPlus, faAngleLeft, faAngleRight,
      faArrowDown, faArrowUp);

    if(this.state.currentUserInfo != null){
      let searchRender = this.renderSearching();
      let searchBt = <div></div>;
      if(this.state.onSearching){
        searchBt = <button className="btn btn-outline-secondary" type="button" onClick={()=>{this.resetSearch();}}><FontAwesomeIcon icon="redo"></FontAwesomeIcon></button>;
      }else{
        searchBt = <button className="btn btn-outline-secondary" type="button" onClick={()=>{this.settingSearchTextWhenClick('searchBox');}}><FontAwesomeIcon icon="search"></FontAwesomeIcon></button>;
      }
  
      let actionBtn = ActionButton.map((btn)=>{
        if(btn.Action.indexOf(this.state.userActionPermission) > -1 && this.state.userGroup.length > 0){
          return(
            <button type="button" className={"mybtn outline-purple-btn"} style={{marginLeft:"5px", marginRight:"5px"}} onClick={()=>{this.changeCurrentState(btn.Title);}}>{btn.Title}</button>
          );
        }
      });
  
      let currentStateNo = 0;
      let actionStateNo = 1;

      let allPin = [];
      let pinMessage = [];
      let pinIndicator = [];
      this.state.allComments.forEach((cm,cIndex)=>{
        if(cm.isPin.indexOf(this.state.currentTab) > -1 || cm.share_to.indexOf(cm.isPin) > -1){
          allPin.unshift(cm);
        }
      });

      allPin.forEach((p, pIndex)=>{
        let contentArr = p.content;
        let pingArr = p.pings;
        if(pingArr.length > 0){
          pingArr.forEach((pUser)=>{
            if(contentArr.indexOf(pUser.fullname) > -1){
              contentArr = contentArr.replace('@'+pUser.fullname,'<input class="tagPing" type="button" value="@'+pUser.fullname+'" />&nbsp');
            }
          });
        }

        let time = this.getdateformat(p.modified);

        let fileRender = <div></div>;
        if(p.file.length > 0){
          let f_Render = p.file.map((file)=>{
            if(file.file_type.indexOf('image') > -1){
              return <img src={file.file_url} style={{width:"100%", maxHeight:"150px", objectFit:"contain", padding:"5px"}} />;
            }else{
              return <div><a href={file.file_url} target="_blank">{file.file_name}</a></div>;
            }
          });
          fileRender = (
            <div style={{marginLeft:'70px', paddingTop:"5px"}}>
              {f_Render}
            </div>
          );
        }

        if(this.state.currentPin == pIndex){
          pinMessage.push(
            <div className={"innerCarousel"} style={{opacity:1,zIndex: 1}}>
              <div className={"thumbtack"}><FontAwesomeIcon icon="thumbtack" /></div>
              <div className="row">
                <div className={"profilePicture"} style={{backgroundImage: `url("${p.profile_picture_url}")`}}></div>
                <div className="col" style={{width:"calc(100% - 50px)"}}>
                  <div 
                    onMouseDown={(e)=>{e.preventDefault(); captureMouseDown = e.pageX; }} 
                    onMouseUp={(e)=>{ e.preventDefault(); this.swiper(e.pageX, pIndex, allPin); }}
                    onTouchStart={(e)=>{e.preventDefault(); captureMouseDown = e.touches[0].pageX;}}
                    onTouchEnd={(e)=>{e.preventDefault(); this.swiper(e.touches[0].pageX, pIndex, allPin);}}
                  >
                    <div className="row">
                      <div className={"col innerCarouselName"} style={{fontSize:"12px"}}>{p.fullname}</div>
                      <div className={"commentTime"}>
                        <div className="row no-gutters">
                          <div style={{marginTop:"2px"}}>{time}</div>
                        </div>
                      </div>
                    </div>
                    <div className={"blockWithText"} style={{fontSize:'12px'}}>
                      <div dangerouslySetInnerHTML={{ __html: contentArr }}></div>
                      {fileRender}
                    </div>
                  </div>
                  <div className="row" style={{margin:'0'}}>
                    <div className={"col btnWithNoBorder font-xs"} onClick={()=>{firstCall = 10; this.changeScrollTo('comment_'+p.id,10,p.parent);}} style={{boxShadow:'-10px -10px 5px 0px #fff', padding:"0", fontSize:"12px"}}>View post...</div>
                  </div>
                </div>
              </div>
            </div>
          );  
          pinIndicator.push(
            <div className={"indicatorActive"}></div>
          );
        }else{
          pinMessage.push(
            <div className={"innerCarousel"} style={{opacity:0,zIndex: 0}}>
              <div className={"thumbtack"}><FontAwesomeIcon icon="thumbtack" /></div>
              <div className="row">
                <div className={"profilePicture"} style={{backgroundImage: `url("${p.profile_picture_url}")`}}></div>
                <div className="col" style={{width:"calc(100% - 50px)"}}>
                  <div className="row">
                    <div className={"col innerCarouselName"} style={{fontSize:"12px"}}>{p.fullname}</div>
                    <div className={"commentTime"}>
                      <div className="row no-gutters">
                        <div style={{marginTop:"2px"}}>{time}</div>
                      </div>
                    </div>
                  </div>
                  <div className={"blockWithText"} style={{fontSize:'12px'}}>
                    <div dangerouslySetInnerHTML={{ __html: contentArr }}></div>
                    {fileRender}
                  </div>
                  <div className="row" style={{margin:'0'}}>
                    <div className={"col btnWithNoBorder font-xs"} onClick={()=>{firstCall = 10; this.changeScrollTo('comment_'+p.id,10, p.parent);}} style={{boxShadow:'-10px -10px 5px 0px #fff', padding:"0"}}>View post...</div>
                  </div>
                </div>
              </div>
            </div>
          );  
          pinIndicator.push(
            <div className={"indicator"} onClick={()=>{this.changeIndicator(pIndex);}}></div>
          );
        }
      });

      let pincss = "";
      let pinHeigh = 0;
      let additionHeight = 0;
      if(pinMessage.length > 0){
        pinHeigh = 108;
        pincss = "carousel";
        additionHeight = 30;
      }

      let postHeigh = `calc(100vh - 145px - ${pinHeigh}px - ${additionHeight}px)`; //team 140 - 260; dev 300 - 450;
      if(this.state.currentState != ''){ currentStateNo = 1; actionStateNo = 0; postHeigh = `calc(100vh - 265px - ${pinHeigh}px - ${additionHeight}px)`; }
      else{ currentStateNo = 0; actionStateNo = 1; postHeigh = `calc(100vh - 145px - ${pinHeigh}px - ${additionHeight}px)`; }
  
      let currentRender = this.renderCurrentState();
      let comments = this.renderComments();
      //, position:'fixed',left:"0",width:"100%",top:"85px"
      //style={{position:'fixed',left:"0",width:"100%",top:"85px", backgroundColor:'rgb(243,242,241)'}}
      let alertScale = 0;
      if(this.state.alertStatus != ''){ alertScale = 1; }

      let alertNewPost =(
        <div className={styles.alertNewPost} style={{display: this.state.displayAlert}}>
          <div onClick={()=>{this.setNewPost();}} className={styles.alertNewPostBtn}>
            {this.state.newTextAlert}
          </div>
        </div>
      );

      let defaultNav = <div></div>;

      let actionHeigh = actionStateNo *  30;

      let header = <div></div>
      if(this.state.parentItem != null){
        let timehead = this._getDateformate(this.state.parentItem.Created);
        let newTag = this.newBadge(this.state.parentItem.Created);
        let star = this.renderStar(this.state.parentItem.AverageRating);
        let temptimehead = timehead.split('-');
        
        let hashtag = this.state.parentItem.Hashtag.map((item)=>{
          return <span className="badge badge-primary" style={{margin:'0px 5px'}}>{item}</span>
        });
        header = (
          <div className="col">
            <div className="row no-gutters">
              <div className="col">
                <div className="row no-gutters"><h2>{this.state.parentTopic}</h2>{star}</div>
              </div>
              <div style={{paddingTop:'12px', paddingRight:"10px"}}>
                <h6>{newTag}{hashtag}</h6>
              </div>
            </div>
            <div className="row no-gutters">
              <div className="col">
                <h5>Author: {this.state.parentUser.Title} - {temptimehead[0]}</h5>
                <h6>Description: {this.state.parentItem.Description}</h6>
              </div>
            </div>
          </div>
        );
      }

      return (
        <div className={ styles.collaboration } 
          // onMouseMove={(e)=>{this.clickToScroll(e);}} 
          onMouseUp={(e)=>{this.setState({scrollCapture: false, startCapture: 0});}}
          onClick={(e)=>{this.closeAll(e);}}
          onWheel={(e)=>{this.changeScroll()}}
        >
          <div className="container-fluid" style={{minHeight:'100vh', backgroundColor:'rgb(243,242,241)'}}>
            <div className={styles.alertLayout} style={{transform: `scale(${alertScale})`}}>
              <div className="alert alert-success">
                {this.state.alertStatus}
              </div>
            </div>
            <div className="row">
              <div className="col-xl">
                <div>
                  <div className="row mb-2">
                    <div onClick={()=>{window.location.href="https://betteritcorp.sharepoint.com/sites/ShareIdea/SitePages/Main.aspx"}} style={{padding:"0px 10px", cursor:'pointer'}}><h2><FontAwesomeIcon icon="caret-left"/></h2></div>
                    {header}
                  </div>
                  <div className="row mb-1">
                    <div className="col">
                      <div className={pincss} style={{height:pinHeigh}}>{pinMessage}</div>
                      <div style={{textAlign:"center", marginTop:"5px"}}>{pinIndicator}</div>
                    </div>
                  </div>
                  <div className="row" style={{height:postHeigh}}>
                    {/*Post Section*/}
                    <div className="container" style={{position:'relative'}}>
                      <div id="baseScroll" className={"scrollBar"} style={{maxHeight: postHeigh}}>
                        <div id="scrollBar" className={"scroll"} style={{height:this.state.scrollH, top:this.state.scroll}} 
                          onMouseDown={(e)=>{this.setState({scrollCapture: true, startCapture: e.pageY});}}  >
                        </div>
                      </div>  
                      <div className="row" id="rowComment" style={{height:postHeigh, overflowY:'auto', padding:"0px 10px"}} onScroll={(e)=>{this.changeScroll();}}>
                        {alertNewPost}
                        <div id="page-comments" className="col" style={{marginTop:'auto', position:'relative', marginBottom: '15px'}}>
                          {/* <div className={styles.agreement}>By using <span style={{color:'red'}}>Crisis Management Platform</span>, you agree to the  <span style={{color:'red'}}>Terms of use</span> and <span style={{color:'red'}}>Privacy statement</span>.</div> */}
                          {comments} 
                        </div>               
                      </div>
                    </div>
                  </div>
                  <hr style={{marginBottom:"0.5rem", marginTop:"0.5rem"}}/>
                  <div className="row mb-2">
                    <div className="col" style={{textAlign:'center', position:'relative'}}>
                      <div className={"actionBtnStyle"} style={{transform:`scale(${actionStateNo})`, height:`${actionHeigh}px`}}>{actionBtn}</div>
                      <div className={"actionRender"} style={{transform:`scale(${currentStateNo})`}}>{currentRender}</div>
                    </div>
                  </div>
                </div>
              </div>

              <div className={"searchLayOut"} id="searchLayoutBlock">
                <div className="row" style={{margin:"0"}}>
                  <div className="col" style={{padding:'0'}}>
                    <div className="input-group">
                      <input type="text" id="searchBox" className="form-control rounded-right" placeholder="Search" onKeyDown={(e)=>{this.settingSearch(e);}}  onChange={()=>{this.settingSearchText('searchBox');}}/>
                      <div className="input-group-append">
                        {searchBt}
                      </div>
                    </div>
                  </div>
                </div>
                {searchRender}
              </div>
            </div>
          </div>
        </div>
      );
    }else{
      return <div></div>;
    }
  }

  private renderStar(avgStar){
    let maxStar = [1,1,1,1,1];
    let avg = avgStar;
    let renderStar = maxStar.map((count)=>{
      if(avg > 0){
        if(avg < 1){
          avg = 0;
          return <span className={styles.star +" "+styles.half}></span>;
        }else{
          avg = avg - 1;
          return <span className={styles.star +" "+ styles.on}></span>;
        }
      }else{
        return <span className={styles.star}></span>;
      }
    });
    return <div>{renderStar}</div>
  }

  private newBadge(dateString){
    var today = new Date();
    var date:Date = new Date(dateString);
    if(today.getDate() == date.getDate() && today.getMonth() == date.getMonth() && today.getFullYear() == date.getFullYear()){
      return <span className="badge badge-danger" style={{margin:'0px 5px'}}>New</span>;
    }else{return <span className="badge badge-danger"></span>; }
  }

  private _getDateformate(dateString:string):string{
   
    var date:Date = new Date(dateString);
    
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var hr = hours < 10 ? '0'+hours : hours; // the hour '0' should be '12'
    var min = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hr + ':' + min;
    return datNames[date.getDay()] + " " + date.getDate()+" "+monthNames[date.getMonth()] + " "+date.getFullYear() + " - " + strTime ; //date.getDate()+"/"+ (date.getMonth()+1) + "/"+date.getFullYear() ;//+" "+strTime;
  }

  public componentDidMount(){
    this.prepPost();
  }

  public async componentDidUpdate(prevProps, prevStates){
    if(this.state.allComments.length != 0 && !this.state.firstRender){
      await this.waiting(500);
      this.setState({
        firstRender: true
      });
    }
    else if(this.state.changeScroll && this.state.firstRender){
      await this.waiting(500);
      this.state.allComments.forEach((alc)=>{
        if(document.getElementById(`content_${alc.id}`) != null){
          if(document.getElementById(`content_${alc.id}`).clientHeight > 90){
            document.getElementById(`seemore_${alc.id}`).style.display = 'block';
          }
        }
      });
      this.scrollToBottom();
      this.setState({
        changeScroll: false
      });
    }
    if(this.state.settingSeemore != ''){
      if(document.getElementById(`seemore_${this.state.settingSeemore}`).style.display == 'none'){
        document.getElementById(`seemore_${this.state.settingSeemore}`).style.display = 'block';
        document.getElementById(`seeless_${this.state.settingSeemore}`).style.display = 'none';
      }else if(document.getElementById(`seemore_${this.state.settingSeemore}`).style.display == 'block'){
        document.getElementById(`seemore_${this.state.settingSeemore}`).style.display = 'none';
        document.getElementById(`seeless_${this.state.settingSeemore}`).style.display = 'block';
      }else{
        if(document.getElementById(`content_${this.state.settingSeemore}`).clientHeight > 90){
          document.getElementById(`seemore_${this.state.settingSeemore}`).style.display = 'block';
          document.getElementById(`seeless_${this.state.settingSeemore}`).style.display = 'none';
        }else{
          document.getElementById(`seemore_${this.state.settingSeemore}`).style.display = 'none';
          document.getElementById(`seeless_${this.state.settingSeemore}`).style.display = 'none';
        }
      }
      this.setState({
        settingSeemore: ''
      });
    }
    
    if(this.state.newCommentChange){
      await this.waiting(500);
      this.state.allComments.forEach((alc)=>{
        if(document.getElementById(`content_${alc.id}`) != null){
          if(document.getElementById(`content_${alc.id}`).clientHeight > 90){
            document.getElementById(`seemore_${alc.id}`).style.display = 'block';
          }
        }
      });
      this.scrollToBottom();
      this.setState({
        newCommentChange: false
      });
    }
  }

  private swiper(e, pIndex, allPin){
    if(captureMouseDown-e < -50){
      captureMouseDown = 0;
      if(pIndex == 0){
        this.setState({
          currentPin: allPin.length-1
        });
      }else{
        this.setState({
          currentPin: pIndex-1
        });
      }
    }
    else if(captureMouseDown - e > 50){
      captureMouseDown = 0;
      if(pIndex == allPin.length-1){
        this.setState({
          currentPin: 0
        });
      }else{
        this.setState({
          currentPin: pIndex+1
        });
      }
    }
  }

  private async prepPost(){
    setInterval(()=>{this.prepNewPost()}, 10*1000);
    // console.log('Wait');
    // await this.waiting(5000);
    // console.log('Start');
    // var i = setInterval(()=>{this.createIntervalComment()}, 15*1000);
    // setTimeout(()=>{console.log('end'); clearInterval(i);}, 10*1000*60);
  }

  private closeAll(e){
    if(document.getElementById('shareBlock') != null){
      if(!document.getElementById('shareBlock').contains(e.target)){
        this.setState({
          shareSelectID: 0,
          shareSelect: [],
          shareWith: [],
          toggleShareID: ''
        });
      }
    }

    if(document.getElementById('flagBlock') != null){
      if(!document.getElementById('flagBlock').contains(e.target)){
        this.setState({
          toggleFlagID: ''
        });
      }
    }

    if(document.getElementById('mobileNav') != null){
      if(!document.getElementById('mobileNav').contains(e.target)){
        this.setState({
          toggleMobileNav: false
        });
      }
    }

    if(document.getElementById('searchLayoutBlock') != null){
      if(!document.getElementById('searchLayoutBlock').contains(e.target)){
        let val = document.getElementById('searchBox') as HTMLInputElement;
        val.value = '';
        this.setState({
          onSearching: false
        });
      }
    }

    if(document.getElementById('Key_Information_default') != null && document.getElementById('Key_Information_mobile') == null){
      if(!document.getElementById('Key_Information_default').contains(e.target)){
        this.setState({
          currentWidget: ''
        });
      }
    }

    if(document.getElementById('Key_Information_mobile') != null){
      if(!document.getElementById('Key_Information_mobile').contains(e.target)){
        if(this.state.toggleMobileNav){
          if(!document.getElementById('mobileNav').contains(e.target)){
            this.setState({
              currentWidget: ''
            });
          }
        }else{
          this.setState({
            currentWidget: ''
          });
        }
      }
    }

    if(document.getElementById('Assign_Task_default') != null && document.getElementById('Assign_Task_mobile') == null){
      if(!document.getElementById('Assign_Task_default').contains(e.target)){
        this.setState({
          currentWidget: '',
          searchTable: '',
          sortTable: '',
          tooltipMobile: ''
        });
      }
    }

    if(document.getElementById('Assign_Task_mobile') != null){
      if(!document.getElementById('Assign_Task_mobile').contains(e.target)){
        if(this.state.toggleMobileNav){
          if(!document.getElementById('mobileNav').contains(e.target)){
            this.setState({
              currentWidget: '',
              searchTable: '',
              sortTable: '',
              tooltipMobile: ''
            });
          }
        }else{
          this.setState({
            currentWidget: '',
            searchTable: '',
            sortTable: '',
            tooltipMobile: ''
          });
        }
      }
    }

    if(document.getElementById('Deactivate_default') != null && document.getElementById('Deactivate_mobile') == null){
      if(!document.getElementById('Deactivate_default').contains(e.target)){
        this.setState({
          currentWidget: ''
        });
      }
    }

    if(document.getElementById('Deactivate_mobile') != null){
      if(!document.getElementById('Deactivate_mobile').contains(e.target)){
        if(this.state.toggleMobileNav){
          if(!document.getElementById('mobileNav').contains(e.target)){
            this.setState({
              currentWidget: ''
            });
          }
        }else{
          this.setState({
            currentWidget: ''
          });
        }
      }
    }

    if(cickedBtn != null){
      if(document.getElementById('toggleBtnUpdate'+cickedBtn.id) != null){
        if(!document.getElementById('toggleBtnUpdate'+cickedBtn.id).contains(e.target)){
          this.closeDiv(e,cickedBtn);
        }
      }
    }
  }

  private async prepNewPost(){
    this.webPnP = new Web(this.baseUrl);
    var jsonComments, jsonTask;
    let pagecomments = await this.webPnP.lists.getByTitle('CrisisCollaboration').items.select('Comments', 'FieldValuesAsText/Comments', 'FilesFolder', 'Id', 'Created', 'Modified')
        .filter(`Title eq 'CrisisCollaboration'`).expand('FieldValuesAsText').get();
    if (pagecomments.length > 0) {
      var tempComments = pagecomments[0].FieldValuesAsText.Comments;

      if (tempComments != undefined && tempComments != null && tempComments !== "") {
        jsonComments = JSON.parse(tempComments);
      }else{
        jsonComments = [];
      }
      let collabID = pagecomments[0].Id;
      let fileFolder = pagecomments[0].FilesFolder;
      if(jsonComments.length == this.state.allComments.length){
        this.setState({
          allComments: jsonComments,
          collabID: collabID,
          fileFolder: fileFolder
        });
      }else{
        let tempJSON = jsonComments;
        let tempCount = 0;
        let tempNewPost = jsonComments.splice(this.state.allComments.length,this.state.newPostComment.length-this.state.allComments.length);
        tempNewPost.forEach((n_post)=>{
          if(n_post.team == this.state.currentTab || n_post.share_to.indexOf(this.state.currentTab) > -1){
            tempCount++;
          }
        });
        let newText = '', newDisplay = 'none';
        if(tempCount > 0){
          newText = `Unread ${tempCount} post. Click here to refresh.`;
          newDisplay = 'block';
        }
        
        this.setState({
          newPostComment: tempJSON,
          collabID: collabID,
          fileFolder: fileFolder,
          displayAlert: newDisplay,
          newTextAlert: newText
        });
      }
    };
  }

  private async setNewPost(){
    var jsonComments: any[];
    let pagecomments = await this.webPnP.lists.getByTitle('CrisisCollaboration').items.select('Comments', 'FieldValuesAsText/Comments', 'FilesFolder', 'Id', 'Created', 'Modified')
        .filter(`Title eq 'CrisisCollaboration'`).expand('FieldValuesAsText').get();
    if (pagecomments.length > 0) {
      var tempComments = pagecomments[0].FieldValuesAsText.Comments;
      if (tempComments != undefined && tempComments != null && tempComments !== "") {
          jsonComments = JSON.parse(tempComments);
          this.setState({
            allComments: jsonComments,
            newPostComment: [],
            newCommentChange: true,
            displayAlert:"none",
            newTextAlert: ''
          });
      };
    };
  }

  private changeIndicator(index){
    this.setState({
      currentPin: index
    });
  }

  public weekDay = ["Sun","Mon","Tue","Wed","Thu","Fri","Sat"] ;
  private getdateformat(dateString:string):string{
    var date:Date = new Date(dateString);
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    var min = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + min + ' ' + ampm;

    let rtnDate = "" ;
    if(date.getFullYear() == todayDate.getFullYear()){
      if(date.getMonth() == todayDate.getMonth()){
        if(date.getDate() == todayDate.getDate() ) rtnDate =  "Today "+ strTime ;
        else if((date.getDate() - todayDate.getDate()) >=  -6 && (date.getDate() - todayDate.getDate()) <= 6) {
          if((date.getDate() - todayDate.getDate()) ==  1) rtnDate = "Tomorrow "+ strTime ;
          else if(date.getDate() - todayDate.getDate() == -1) rtnDate = "Yesterday "+ strTime ;
          else rtnDate =  this.weekDay[date.getDay()] + ", "+date.getDate()+"/"+(date.getMonth()+1) ;
        }
        else{
          rtnDate = date.getDate()+"/"+(date.getMonth()+1) ;
        } 
      } // Same Month
      else{
        rtnDate = date.getDate()+"/"+(date.getMonth()+1) ;
      } // else same month
    } // same Year
    else{
      rtnDate = date.getDate()+"/"+(date.getMonth()+1)+ "/"+date.getFullYear() ;
    }  // else same Year
  
    return rtnDate ; //date.getDate()+"/"+ (date.getMonth()+1) + "/"+date.getFullYear() +" "+strTime;
      //this.monthNames[date.getMonth()]+" "+ date.getDate() + ", "+date.getFullYear() ; //date.getDate()+"/"+ (date.getMonth()+1) + "/"+date.getFullYear() ;//+" "+strTime;
  }

  private async scrollToBottom(){
    await this.waiting(50);
    let content = document.getElementById('rowComment');
    content.scrollTop = content.scrollHeight;
    let s_H =  content.clientHeight - (content.scrollHeight - content.clientHeight);
    let scroll = content.scrollTop;
    if(s_H < 30){
      s_H = 30;
      if(scroll > 0){
        scroll = (content.scrollTop/(content.scrollHeight-content.clientHeight))*(content.clientHeight-30);
      }
    }
    let scrollBar = document.getElementById('scrollBar');
    let baseScrollBG = document.getElementById('baseScroll');    
    let diffContent = content.clientHeight - content.scrollHeight;
    if(diffContent < 5 && diffContent > -5){ 
      scrollBar.style.display = 'none'; baseScrollBG.style.backgroundColor = 'rgb(243,242,241)'; 
    }else{
      scrollBar.style.display = 'block'; baseScrollBG.style.backgroundColor = 'white'; 
    }
    this.setState({
      scrollH: s_H,
      scroll: scroll
    });
  }

  private async changeScroll(){
    await this.waiting(50);
    let content = document.getElementById('rowComment');
    let s_H =  content.clientHeight - (content.scrollHeight - content.clientHeight);
    let scroll = content.scrollTop;
    if(s_H < 30){
      s_H = 30;
      if(scroll > 0){
        scroll = (content.scrollTop/(content.scrollHeight-content.clientHeight))*(content.clientHeight-30);
      }
    }
    let scrollBar = document.getElementById('scrollBar');
    let baseScrollBG = document.getElementById('baseScroll');
    let diffContent = content.clientHeight - content.scrollHeight;
    if(diffContent < 5 && diffContent > -5){ 
      scrollBar.style.display = 'none'; baseScrollBG.style.backgroundColor = 'rgb(243,242,241)'; 
      this.setState({
        scrollH: s_H,
        scroll: scroll
      });
    }else{ 
      scrollBar.style.display = 'block'; baseScrollBG.style.backgroundColor = 'white'; 
      this.setState({
        scrollH: s_H,
        scroll: scroll
      });
    }
  }

  private waiting(ms){
    return new Promise(resolve => setTimeout(resolve,ms));
  }

  private async changeScrollTo(toID, called, blockContent){
    await this.waiting(50);
    let toClientID = document.getElementById(toID).offsetTop - 10;
    let content = document.getElementById('rowComment');
    if(toClientID < 0){ content.scrollTop = 0; }else{ content.scrollTop = (toClientID - 10); }
    let s_H =  content.clientHeight - (content.scrollHeight - content.clientHeight);
    let scroll = content.scrollTop;
    if(s_H < 30){
      s_H = 30;
      if(scroll > 0){
        scroll = (content.scrollTop/(content.scrollHeight-content.clientHeight))*(content.clientHeight-30);
      }
    }
    let scrollBar = document.getElementById('scrollBar');
    let baseScrollBG = document.getElementById('baseScroll');
    let diffContent = content.clientHeight - content.scrollHeight;
    if(diffContent < 5 && diffContent > -5){ 
      scrollBar.style.display = 'none'; baseScrollBG.style.backgroundColor = 'rgb(243,242,241)'; 
    }else{
      scrollBar.style.display = 'block'; baseScrollBG.style.backgroundColor = 'white'; 
    }
    this.setState({
      scrollH: s_H,
      scroll: scroll
    });

    let defaultColor = '';
    if(blockContent == ''){ defaultColor = '#fff'; }else{ defaultColor = '#e6e6e6'; }
    if(called != 50){
      document.getElementById(toID).style.backgroundColor = '#fff5d9';
    }
    let seemoreDiv = document.getElementById(`seemore_${toID}`)
    if(seemoreDiv != undefined){
      if(document.getElementById(`seemore_${toID}`).style.display == 'none'){
        document.getElementById(`seelessb_${toID}`).style.boxShadow = '0px -10px 5px 0px rgba(255,245,217,1)';
      }else{
        document.getElementById(`seemoreb_${toID}`).style.boxShadow = '0px -10px 5px 0px rgba(255,245,217,1)';
      }
    }
    await this.waiting(1000);
    document.getElementById(toID).style.backgroundColor = defaultColor;
    if(seemoreDiv != undefined){
      if(document.getElementById(`seemore_${toID}`).style.display == 'none'){
        document.getElementById(`seelessb_${toID}`).style.boxShadow = '0px -10px 5px 0px rgba(255,255,255,1)';
      }else{
        document.getElementById(`seemoreb_${toID}`).style.boxShadow = '0px -10px 5px 0px rgba(255,255,255,1)';
      }
    }
  }

  // private clickToScroll(e){
  //   if(this.state.scrollCapture){
  //     let content = document.getElementById('rowComment');
  //     if(e.pageY - content.getBoundingClientRect().top < 537 && e.pageY - content.getBoundingClientRect().top > 0){
  //       content.scrollTo(content.scrollLeft,e.pageY - content.getBoundingClientRect().top);
  //       this.setState({
  //         scroll: content.scrollTop
  //       });
  //     }
  //   }
  // }
  
  private async checkAndCreateList() {
    await this.checkListExists();
    await this.checkLogExist("_Log");
    await this.checkLogExist("_Recurrent_Log");
    await this.InitialComments();
  }

  private renderComments(){

    let allPinCount = -1;

    let render = this.state.allComments.map((comment,index)=>{
      
      if(comment.parent == '' && (comment.team == this.state.currentTab || comment.share_to.indexOf(this.state.currentTab) > -1)){
        let time = this.getdateformat(comment.modified);

        let suggestDisplay = <div></div>;
        if(this.state.suggestBlockPosition != null && this.state.suggestBlockID == "textareaReply_"+comment.id){ 
          let siteUser = this.state.filterSuggest.map((user)=>{
            if(user.siteGroup == 'No'){
              return (
                <div className={"row suggestItem"} onClick={()=>{this.settingMention(user, "textareaReply_"+comment.id,'unlimit');}}>
                  <div className={styles.suggestProfile} style={{backgroundImage: `url("${user.profile_picture_url}")`, marginRight:"10px"}}></div>
                  <div>
                    <div>
                      <b>{user.fullname}</b>
                    </div>
                    <div className={styles.suggestEmail}>
                      {user.email}
                    </div>
                  </div>
                </div>
              );
            }else{
              return (
                <div className={"row suggestItem"}  onClick={()=>{this.settingMention(user, "textareaReply_"+comment.id,'unlimit');}}>
                  <div className={styles.suggestProfile}><FontAwesomeIcon icon="users" style={{width:'100%',height:"100%"}} /></div>
                  <div>
                    <div style={{marginLeft:'10px'}}>
                      <b>{user.fullname}</b>
                    </div>
                  </div>
                </div>
              );
            }
          });

          let noUser = <div></div>;
          if(this.state.filterSuggest.length == 0){
            noUser = (
              <div className={"row suggestItem"}>
                <div style={{textAlign:"center"}}>
                  We didn't find any matches.
                </div>
              </div>
            );
          }

          suggestDisplay = (
            <div className={"suggestBlock"} style={{left:'100px', bottom: '90px',zIndex:100}}>
              <div className={"suggestHeader"}>Suggestions</div>
              {siteUser}
              {noUser}
            </div>
          );
          
        }else{
          suggestDisplay = <div></div>; 
        }
        let taskStatus = <div></div>;
        let replyTextBlock = <div></div>;
        let replyButton = <button title="Reply this post." type="button" className={"btnWithNoBorder"} onClick={()=>{this.setState({toggleReply:true, parentPost: comment.id});}}><FontAwesomeIcon icon="reply" /> Reply</button>;
        if(this.state.onReplyLoading.indexOf(comment.id) == -1){
          if(this.state.toggleReply && this.state.parentPost == comment.id){
            let fileDispaly = <div style={{marginTop:'10px'}}><span>No file selected.</span></div>;
            if(this.state.attachmentSelect != null && this.state.attachmentSelect.length > 0){
              if(this.state.attachmentSelect.length == 1){
                fileDispaly = <div style={{marginTop:'10px'}}><span>{this.state.attachmentSelect.length} file selected.</span>: {this.state.attachmentSelect[0].name}</div>;
              }else{
                fileDispaly = <div style={{marginTop:'10px'}}><span>{this.state.attachmentSelect.length} files selected.</span></div>;
              } 
            }
            replyTextBlock = (
              <div className="row no-gutters" style={{margin:'0px .5rem 0px 60px', textAlign:"left"}}  id="replySectionBlock">
                <div className={"profilePicture"} style={{backgroundImage: `url("${this.state.currentUserInfo.Picture}")`, marginRight:'10px'}}></div>
                  <div className="col" style={{marginTop:"5px"}}>
                    <div id={"textareaReply_"+comment.id} contentEditable className={"textAreaStyle"} 
                      onKeyUp={(e)=>{this.changeMentionTextArea(e, "textareaReply_"+comment.id,'current_team','unlimit');}} 
                      onClick={(e)=>{this.setState({commentText: true});}}
                      onBlur={(e)=>{if(document.getElementById("textareaReply_"+comment.id).innerHTML == ''){this.setState({commentText: false});}}}
                    >
                    </div>
                  <div className="row" style={{width:'100%', textAlign:"right"}}>
                    <input id="c_r_Attachment" type="file" multiple={true} style={{display:'none'}} onChange={(e)=>{this.setState({attachmentSelect: e.target.files});}} />
                    <div className="col">{fileDispaly}</div>
                    <button className="btnWithNoBorder" type="button" style={{marginRight:'5px', marginTop:'5px'}} onClick={()=>{ document.getElementById('c_r_Attachment').click(); }}><FontAwesomeIcon icon="paperclip"/></button>
                    <button className={"mybtn purple-btn"} style={{marginTop:"5px"}} onMouseDown={()=>{this.createReplyJSON("textareaReply_"+comment.id, 'reply', comment, index); let temp = this.state.onReplyLoading; temp.push(comment.id); this.setState({onReplyLoading:temp});}}><FontAwesomeIcon icon="paper-plane" /> Send</button>;
                  </div>
                </div>
              </div>
            );
            replyButton = <div></div>;
          }
          else{
            replyTextBlock = <div></div>;
            replyButton = <button title="Reply this post." type="button" className={"btnWithNoBorder"} onClick={()=>{this.setState({toggleReply:true, parentPost: comment.id});}}><FontAwesomeIcon icon="reply" /> Reply</button>;
          }
        }else{
          replyButton = (
            <div className="row" style={{margin:"0px 5px"}}>
              <div className="col"></div>
              <div style={{color:"#6264a7", marginRight:"5px"}}>Wait a moment please...</div>
              <div className="lds-ring-mini"><div></div><div></div><div></div><div></div></div>
            </div>
          );
        }

        let fileRender = <div></div>;
        if(comment.file.length > 0){
          let f_Render = comment.file.map((file)=>{
            if(file.file_type.indexOf('image') > -1){
              return <img src={file.file_url} style={{width:"100%", maxHeight:"150px", objectFit:"contain", padding:"5px"}} />;
            }else{
              return <div style={{wordBreak:"break-all"}}><a href={file.file_url} target="_blank">{file.file_name}</a></div>;
            }
          });
          fileRender = (
            <div style={{paddingTop:"5px"}}>
              {f_Render}
            </div>
          );
        }

        let reply = [];

        this.state.allComments.forEach((s_comment)=>{
          if(s_comment.parent == comment.id){

            let seemoreBlock = <div id={`seemore_${s_comment.id}`} className={"seemoreBlock"}><button id={`seemoreb_${s_comment.id}`} type="button" onClick={()=>{this.expandHeight(s_comment.id);}} className={"btnWithNoBorder subseemoreBtn"} style={{cursor:"pointer", width:"100%"}}>See more...</button></div>;
            let seelessBlock = <div id={`seeless_${s_comment.id}`} className={"seelessBlock"}><button id={`seelessb_${s_comment.id}`} type="button" onClick={()=>{this.lessHeigh(s_comment.id);}} className={"btnWithNoBorder seelessBtn"} style={{cursor:"pointer", width:"100%"}}>See less...</button></div>;
            
            let s_contentArr = s_comment.content;
            let s_pingArr = s_comment.pings;
            if(s_pingArr.length > 0){
              s_pingArr.forEach((s_pUser)=>{
                if(s_contentArr.indexOf(s_pUser.fullname) > -1){
                  s_contentArr = s_contentArr.replace('@'+s_pUser.fullname,'<input type="button" class="tagPing" value="@'+s_pUser.fullname+'"/>&nbsp');
                }
              });
            }

            let s_fileRender = <div></div>;
            if(s_comment.file.length > 0){
              let s_f_Render = s_comment.file.map((s_file)=>{
                if(s_file.file_type.indexOf('image') > -1){
                  return <img src={s_file.file_url}  style={{width:"100%", maxHeight:"150px", objectFit:"contain", padding:"5px"}} />;
                }else{
                  return <div style={{wordBreak:"break-all"}}><a href={s_file.file_url} target="_blank">{s_file.file_name}</a></div>;
                }
              });
              s_fileRender = (
                <div>
                  {s_f_Render}
                </div>
              );
            }

            let s_time = this.getdateformat(s_comment.modified);
            reply.push(
              <li className={"comments subComments"} id={`comment_${s_comment.id}`}>
                <div className={"commentWrapper"} style={{boxShadow:'none',marginBottom:'0',backgroundColor:'transparent'}}>
                  <div className={"commentProfile"} style={{backgroundImage: `url("${s_comment.profile_picture_url}")`}}></div>
                  <div className="row" style={{margin:"0"}}>
                    <div className={"name col-md"}>{s_comment.fullname}</div>
                    <div className={"commentTime"}><div>{s_time}</div></div>
                  </div>
                  <div className={"wrapper"}>
                    <div className={"content"} id={`content_${s_comment.id}`} dangerouslySetInnerHTML={{ __html: s_contentArr }}></div>
                    {s_fileRender}
                    {seemoreBlock}
                    {seelessBlock}
                  </div>
                </div>
              </li>
            );
          }
        });

        let contentArr = comment.content;
        let pingArr = comment.pings;
        if(pingArr.length > 0){
          pingArr.forEach((pUser)=>{
            if(contentArr.indexOf(pUser.fullname) > -1){
              contentArr = contentArr.replace('@'+pUser.fullname,'<input type="button" class="tagPing" value="@'+pUser.fullname+'" />&nbsp');
            }
          });
        }

        let pinblock = <div></div>;
        let pinBorder = '0px solid transparent';
        let pinMargin = '0';
        if(comment.isPin.indexOf(this.state.currentTab) > -1){
          allPinCount = allPinCount+1;
          if(this.state.userActionPermission == 'Advance action'){
            pinblock = <div className={"pinBtnActive"} onClick={()=>{this.setPin(index, this.state.currentTab, allPinCount);}}><FontAwesomeIcon icon="thumbtack"/> Pinned</div>;
            pinBorder = '5px solid #e05745'; pinMargin = "-5px";
          }else { pinblock = <div className={"pinBtnActive"} style={{cursor:'default'}}><FontAwesomeIcon icon="thumbtack" /> Pinned</div>; pinBorder = '5px solid #e05745'; pinMargin = "-5px"; }
        }else{ 
          if(this.state.userActionPermission == 'Advance action'){
            pinblock = <div className={"pinBtn"} onClick={()=>{this.setPin(index, this.state.currentTab, allPinCount);}}><FontAwesomeIcon icon="thumbtack"/> Pin to top</div>;
          }else { pinblock = <div></div>; }
        }


        let flagblock = <div></div>;
        if(this.state.userActionPermission == 'Advance action'){
          let flagTooltip = <div></div>;
          if(this.state.toggleFlagID == comment.id){
            let txtFlag = "";
            let tempStart = new Date();
            let tempEnd = new Date();
            tempStart.setUTCHours(tempStart.getUTCHours()+7);
            tempEnd.setUTCHours(tempEnd.getUTCHours()+7);
            let startVal = tempStart.toJSON().slice(0,19);
            let endVal = tempEnd.toJSON().slice(0,19);
            if(comment.start_date != ""){
              startVal = comment.start_date;
            }
            if(comment.end_date != ""){
              endVal = comment.end_date;
            }
            flagTooltip = (
              <div className={"flagKeyTooltip"} style={{right:"-70px", width:"300px", left:"auto", bottom:"auto", top:"15px"}}>
                <div className="caret-up-style" style={{marginRight:"80px", textAlign:'right', marginBottom:"-8px"}}><FontAwesomeIcon icon="caret-up" /></div>
                <div className={"toggle-box font-xs"} id="flagBlock">
                  <div className="row mb-2" style={{margin:"0"}}>
                    <div className={"flagFormTitle"}><span style={{color:"red"}}>*</span> Title</div>
                    <div className="col" style={{paddingRight:"0", paddingLeft:"5px"}}><input id="key_title" className={"flagFormInput"} type="text" defaultValue={comment.key_title} /></div>
                  </div>
                  <div className="row mb-2" style={{margin:"0"}}>
                    <div className={"flagFormTitle"}><span style={{color:"red"}}>*</span> Group</div>
                    <div className="col"  style={{paddingRight:"0", paddingTop:"3px", paddingLeft:"0px"}}>
                      <input type="radio" checked={this.state.selected_lane == 'IT'} style={{marginRight:"5px", marginLeft:"5px"}} onChange={()=>this.changeLane('IT')} />IT
                      <input type="radio" checked={this.state.selected_lane == "Business"} style={{marginRight:"5px", marginLeft:"5px"}} onChange={()=>this.changeLane('Business')} />Business
                      <input type="radio" checked={this.state.selected_lane == "Stakeholder"} style={{marginRight:"5px", marginLeft:"5px"}} onChange={()=>this.changeLane('Stakeholder')} />Ext.Stakeholder
                    </div>
                  </div>
                  <div className="row mb-2" style={{margin:"0"}}>
                    <div className={"flagFormTitle"} style={{padding:'0px 10px', marginRight:"10px"}}>Time</div>
                    <div><input type="datetime-local" className={"flagFormInput"} id="key_start" defaultValue={startVal} /></div>
                  </div>  
                  {/* <div className="row mb-2" style={{margin:"0"}}>
                    <div className={"flagFormTitle"} style={{padding:'0px 10px', marginRight:"10px"}}>To</div>
                    <div><input type="datetime-local" className={"flagFormInput"} id="key_end" defaultValue={endVal} /></div>
                  </div> */}
                  <div className="row" style={{margin:"0"}}>
                    <div className="col"></div>
                    <button type="button" className={"mybtn purple-btn"} onClick={()=>{ this.updateFlagKey(comment.id,txtFlag); }}>Save</button>
                  </div>
                </div>
              </div>
            );
          }

          if(comment.key_type != ''){
            let classStyle = "";
            if(comment.key_type == "Key Action"){ classStyle = "key-action"; }else if(comment.key_type == "Key Issue"){ classStyle = "key-issue"; }
            else if(comment.key_type == "Key Event"){ classStyle = "key-event"; }
            flagblock = (
              <div style={{position:'relative'}}>
                {flagTooltip}
                <button type="button" className="btnWithNoBorder font-xs" style={{marginRight:"5px", display:"inline-flex"}}
                  onClick={()=>{
                    if(this.state.toggleFlagID != comment.id){
                      this.setState({
                        toggleFlagID: comment.id,
                        currentWidget: '',
                        toggleShareID: '',
                        selected_lane: comment.lane
                      });
                    }else{
                      this.setState({
                        toggleFlagID: '',
                        currentWidget: '',
                        toggleShareID: '',
                        selected_lane: comment.lane
                      });
                    }
                  }}
                ><div className={classStyle} style={{marginRight:'5px'}}></div> {comment.key_type}</button> 
              </div>
            );
          }else{
            flagblock = (
              <div style={{position:'relative'}}>
                {flagTooltip}
                <button type="button" className={"btnWithNoBorder font-xs"} style={{marginRight:"5px"}}
                  onClick={()=>{
                    if(this.state.toggleFlagID != comment.id){
                      this.setState({
                        toggleFlagID: comment.id,
                        currentWidget: '',
                        toggleShareID: '',
                        selected_lane: 'IT'
                      });
                    }else{
                      this.setState({
                        toggleFlagID: '',
                        currentWidget: '',
                        toggleShareID: '',
                        selected_lane: 'IT'
                      });
                    }
                  }}
                  title="Flag key information."
                ><FontAwesomeIcon icon="flag"/> Flag key.</button> 
              </div>
            );
          }
        }else{ 
          if(comment.key_type != ''){
            let classStyle = "";
            if(comment.key_type == "Key Action"){ classStyle = "key-action"; }else if(comment.key_type == "Key Issue"){ classStyle = "key-issue"; }
            else if(comment.key_type == "Key Event"){ classStyle = "key-event"; }
            flagblock = (
              <div style={{position:'relative'}}>
                <button type="button" className="btnWithNoBorder" style={{marginRight:"5px", display:"inline-flex"}}><div className={classStyle}></div> {comment.key_type}</button> 
              </div>
            );
          }else{
            flagblock = (
              <div style={{position:'relative'}}>
              </div>
            );
          } 
        }

        let seemoreBlock = <div id={`seemore_${comment.id}`} className={"seemoreBlock"}><button id={`seemoreb_${comment.id}`} type="button" onClick={()=>{this.expandHeight(comment.id);}} className={"btnWithNoBorder seemoreBtn"} style={{cursor:"pointer", width:"100%"}}>See more...</button></div>;
        let seelessBlock = <div id={`seeless_${comment.id}`} className={"seelessBlock"}><button id={`seelessb_${comment.id}`} type="button" onClick={()=>{this.lessHeigh(comment.id);}} className={"btnWithNoBorder seelessBtn"} style={{cursor:"pointer", width:"100%"}}>See less...</button></div>;

        return (
          <li className={"comments"}>
            <div className={"pinShareSection"}>
              {pinblock}
            </div>
            <div className={"commentWrapper"} id={`comment_${comment.id}`} style={{borderLeft: pinBorder, marginLeft: pinMargin}}>
              <div className={"commentProfile"} style={{backgroundImage: `url("${comment.profile_picture_url}")`}}></div>
                <div className="row" style={{margin:"0"}}>
                  <div className={"name col-md"}>{comment.fullname}</div>
                  <div className={"commentTime"}><div className="row no-gutters">{flagblock} <div style={{marginTop:"2px"}}>{time}</div></div></div>
                </div>
              <div className={"wrapper"}>
                <div className={"content"} id={`content_${comment.id}`} dangerouslySetInnerHTML={{ __html: contentArr }}>
                </div>
                {seemoreBlock}
                {seelessBlock}
                {fileRender}
              </div>
              <div><ul style={{listStyle:'none',backgroundColor:'#f5f5f5',borderTop:'1px solid gainsboro', padding:"0"}}>{reply}</ul></div>
              <div className={"action"}>
                {suggestDisplay}
                {replyTextBlock}
                {replyButton}
              </div>
            </div>
          </li>
        );
      }
    });
    return(
      <div>
        <ul className={"commentSection"}>
          {render}
        </ul>
      </div>
    );
  }

  private changeLane(lane){
    this.setState({
      selected_lane: lane
    });
  }

  private expandHeight(id){
    document.getElementById(`content_${id}`).style.maxHeight = '100%';
    this.setState({
      settingSeemore: id
    });
  }

  private lessHeigh(id){
    document.getElementById(`content_${id}`).style.maxHeight = '100px';
    this.setState({
      settingSeemore: id
    });
  }

  private async updateFlagKey(id,tempFlag){
    let key_title = document.getElementById('key_title') as HTMLInputElement;
    let key_lane = this.state.selected_lane;
    let key_start = document.getElementById('key_start') as HTMLInputElement;
    // let key_end = document.getElementById('key_end') as HTMLInputElement;
    let key_flag = tempFlag;
    if(this.state.flagKeySelected != ""){
      key_flag = this.state.flagKeySelected;
    }

    if(key_title.value == '' || key_flag == ''){
      alert('Please insert required field');
    }else{
      let time = new Date().toISOString();
      if(key_start.value == ''){ key_start.value = time; }
      // if(key_end.value == ''){ key_end.value = time; }
      this.webPnP = new Web(this.baseUrl);

      //let rows = rows ;
      let strUpdate;
      let index_update;
      this.state.allComments.forEach((cm,index)=>{
        if(cm.id == id){
          cm.key_title = key_title.value;
          cm.lane = key_lane;
          cm.start_date = key_start.value;
          cm.end_date = '';
          cm.key_type = key_flag;
          strUpdate = cm;
          index_update = index;
        }
      });
      
      await this.updateFlagPinShareItems(this.pageurl, strUpdate, index_update);
      await this.getPostComments(this.pageurl,this.state.currentUserInfo);
      this.setState({
        toggleFlagID: '',
        flagKeySelected: '',
        selected_lane: 'IT'
      });
    }
  }

  public updateFlagPinShareItems = async (pageurl, comment, index) => {
    this.webPnP = new Web(this.baseUrl);
    let jsonComments = [];
    let pagecomments = await this.webPnP.lists.getByTitle(this.pageName).items.select('Comments', 'FieldValuesAsText/Comments', 'FilesFolder', 'Id', 'Created', 'Modified')
        .filter(`Title eq '${pageurl}'`).expand('FieldValuesAsText').get();
    if (pagecomments.length > 0) {
      var tempComments = pagecomments[0].FieldValuesAsText.Comments;
      jsonComments = JSON.parse(tempComments);
      jsonComments[index] = comment;
      await this.webPnP.lists.getByTitle(this.pageName).items.getById(this.state.collabID).update({
        Comments: JSON.stringify(jsonComments)
      });
    }
  }

  private async setPin(id, team, allPinCount){
    let t_Comment = this.state.allComments;
    let tempComments;
    if(t_Comment[id].isPin.indexOf(team) > -1){
      let cutIndex = t_Comment[id].isPin.indexOf(team);
      t_Comment[id].isPin.splice(cutIndex,1);
      let c_Pin = 0;
      if(this.state.currentPin > 0){
        c_Pin = this.state.currentPin - 1;
      }
      tempComments = t_Comment[id];
      this.setState({
        allComments: t_Comment,
        currentPin: c_Pin
      });
    }else{
      t_Comment[id].isPin.push(team);
      let c_Pin = 0;
      if(this.state.currentPin > 0){
        c_Pin = this.state.currentPin - 1;
      }
      tempComments = t_Comment[id];
      this.setState({
        allComments: t_Comment,
        currentPin: c_Pin
      });
    }
    await this.changeScroll();
    await this.updateFlagPinShareItems(this.pageName,tempComments,id);
    await this.getPostComments(this.pageurl,this.state.currentUserInfo);
  }

  private async InitialComments(){
    let tempUrl = window.location.href.split('#');
    this.pageurl = atob(tempUrl[1]);
    await this.getCurrentUserInfo();
    await this.getPostComments(this.pageurl,this.state.currentUserInfo);
    await this.updateFilePath();
  }

  private highlight(content, searchText){
    var index = ((content).toLowerCase()).indexOf((this.state.searchTxt).toLowerCase());
    if (index >= 0) { 
      let preDotted = '', postDotted = '';
      if(index - 20 > 0){
        preDotted = '...'
      }
      if(index + 20 >= searchText.length){
        postDotted = '...'
      }
      content = preDotted+content.substring(index-20,index) + "<span style='background-color: wheat'>" + content.substring(index,index+searchText.length) + "</span>" + content.substring(index+searchText.length,index + 20)+postDotted;
    }
    return content;
  }


  private renderSearching(){
    if(this.state.onSearching){
      let translateX = '120%';
      if(this.state.onSearching){
        translateX = '0';
      }else{
        translateX = '120%';
      }
  
      let findPostSearchResults = [], findPostCount = 0;
      let findFileSearchResults = [], findFileCount = 0;
  
      let postPagiRender = [];
      let postPagiCount = 0;
  
      let filePagiRender = [];
      let filePagiCount = 0;
      
      let postPage = [];
      let filePage = [];
  
      // let findPeopleSearchResults = [];
      this.state.allComments.forEach((cm, index)=>{
        if(cm.team == this.state.currentTab || cm.share_to.indexOf(this.state.currentTab) > -1){
          if(((cm.content).toLowerCase()).indexOf((this.state.searchTxt).toLowerCase()) > -1){
            postPagiCount = postPagiCount + 1;
            if(postPagiCount == 1){
              postPage.push(postPage.length);
            }else if(postPagiCount == 5){
              postPagiCount = 0;
            }
            findPostCount = findPostCount + 1;
            if(findPostSearchResults.length < 5  && this.state.onSearching && findPostCount > this.state.pageMessage*5 && findPostCount <= ((this.state.pageMessage*5)+5)){
              let border = '1px solid darkgrey';
              if(findPostSearchResults.length == 0){border = 'none';}else{ border = '1px solid darkgrey'; }
              findPostSearchResults.push(
                <div className="searchResultBox" onClick={()=>{firstCall = 10; this.changeScrollTo('comment_'+cm.id,10, cm.parent);}}>
                  <div className="row font-xs" style={{padding:"5px", borderTop: border, margin:"0"}}>
                    <div className={"profilePicture"} style={{backgroundImage: `url("${cm.profile_picture_url}")`, width:"30px", height:"30px"}}></div>
                    <div className="col">
                      <div>
                        <div className="row" style={{margin:"0"}}>
                          <div className="col" style={{padding:"0", fontWeight:'bold'}}>{cm.fullname}</div>
                          <div>{this.getdateformat(cm.created)}</div>
                        </div>
                       </div>
                      <div dangerouslySetInnerHTML={{__html: this.highlight(cm.content,this.state.searchTxt)}}></div>
                    </div>
                  </div>
                </div>
              );
            }
          }
    
          if(cm.file.length > 0){
            cm.file.forEach((cmf)=>{
              if(((cmf.file_name).toLowerCase()).indexOf((this.state.searchTxt).toLowerCase()) > -1){
                filePagiCount = filePagiCount + 1;
                if(filePagiCount == 1){
                  filePage.push(filePage.length);
                }else if(filePagiCount == 5){
                  filePagiCount = 0;
                }
                findFileCount = findFileCount + 1;
                if(findFileSearchResults.length < 5  && this.state.onSearching  && findFileCount > this.state.pageFile*5 && findFileCount <= ((this.state.pageFile*5)+5)){
                  let border = '1px solid darkgrey';
                  if(findFileSearchResults.length == 0){border = 'none';}else{ border = '1px solid darkgrey'; }
                  findFileSearchResults.push(
                    <div className="searchResultBox" onClick={()=>{firstCall = 10; this.changeScrollTo('comment_'+cm.id,10, cm.parent);}}>
                      <div className="row font-xs" style={{padding:"5px", borderTop: border}}>
                        <div className={"profilePicture"} style={{backgroundImage: `url("${cm.profile_picture_url}")`, width:"30px", height:"30px"}}></div>
                        <div className="col">
                          <div>
                            <div className="row" style={{margin:"0"}}>
                              <div className="col" style={{padding:"0", fontWeight:'bold'}}>{cm.fullname}</div>
                              <div>{this.getdateformat(cm.created)}</div>
                            </div>
                          </div>
                          <div dangerouslySetInnerHTML={{__html: this.highlight(cmf.file_name,this.state.searchTxt)}}></div>
                        </div>
                      </div>
                    </div>
                  );
                }
              }
            });
          }
        }
      });
  
      let renderResults = [];
  
      if(findPostSearchResults.length > 0){
        postPagiRender = postPage.map((pPage)=>{
          return (
            <div className="btnWithNoBorder" onClick={()=>{this.changePage('post', pPage);}} style={{cursor:"pointer", marginLeft:'5px'}}>{pPage+1}</div>
          );
        });
      }
      if(findFileSearchResults.length > 0){
        filePagiRender = filePage.map((fPage)=>{
          return (
            <div className="btnWithNoBorder" onClick={()=>{this.changePage('file', fPage);}} style={{cursor:"pointer", marginLeft:'5px'}}>{fPage+1}</div>
          );
        });
      }

      let message = <div></div>;
      if(findPostSearchResults.length > 0){
        message = (
          <div>
            <div className={"groupHeaderActive"}>Message <span className="badge badge-danger">{findPostCount} items</span></div>
            {findPostSearchResults}
            <div className="row" style={{margin:"0", float:'right'}}>Pages: {postPagiRender}</div>
          </div>
        );
      }

      let file = <div></div>;
      if(findFileSearchResults.length > 0){
        file = (
          <div>
            <div className={"groupHeaderActive"}>File <span className="badge badge-danger">{findFileCount} items</span></div>
            {findFileSearchResults}
            <div className="row" style={{margin:"0", float:'right'}}>Pages: {filePagiRender}</div>
          </div>
        );
      }
  
      return(
        <div style={{transform: `translateX(${translateX})`}} className={"searchResultLayoutBlock"}>
          <div>
            <div className="row mb-2 font-s">
              <div className="col" style={{fontWeight:"bold"}}>
                Search results
              </div>
            </div>
            {message}
            {file}
          </div>
        </div>
      );
    }else{
      return (
        <div style={{transform: `translateX(120%)`}} className={"searchResultLayoutBlock"}>
          <div></div>
        </div>
      );
    }
  }

  private changePage(type, page){
    if(type=="post"){
      this.setState({
        pageMessage: page
      });
    }else{
      this.setState({
        pageFile: page
      });
    }
  }

  private changeCurrentState(state){
    this.setState({
      currentState: state,
      changeScroll: true,
      enablePing: false,
      suggestBlockPosition: null,
      filterSuggest: [],
      suggestSearch: '',
      suggestBlockID: '',
      parentPost: '',
      attachmentSelect: [],
      suggestSelect: [],
      commentText: false
    });
  }

  private renderCurrentState(){
    switch(this.state.currentState){
      case 'New Post': 
      if(this.state.onLoading == ''){
        let span = "";
        if(!this.state.commentText){ span = "Post something..."; }
        else{ span = ""; }
        
        let send_button = <button type="button" style={{marginTop:"5px"}} className={"mybtn purple-btn"}><FontAwesomeIcon icon="paper-plane" /> Send</button>;
        if(!this.state.commentText && this.state.attachmentSelect.length == 0){ send_button = <button type="button" style={{marginTop:"5px"}} className={"sendDisable"}><FontAwesomeIcon icon="paper-plane" /> Send</button>; }
        else{ send_button = <button type="button" style={{marginTop:"5px"}} className={"mybtn purple-btn"}  onClick={()=>{this.createCommentJSON(); this.setState({onLoading:this.state.currentState});}}><FontAwesomeIcon icon="paper-plane" /> Send</button>; }

        let suggestDisplay = <div></div>;
        if(this.state.suggestBlockPosition != null){ 
          let siteUser = this.state.filterSuggest.map((user)=>{
            if(user.siteGroup == 'No'){
              return (
                <div className={"row suggestItem"} onClick={()=>{this.settingMention(user, 'textarea','unlimit');}}>
                  <div className={styles.suggestProfile} style={{backgroundImage: `url("${user.profile_picture_url}")`, marginRight:"10px"}}></div>
                  <div>
                    <div>
                      <b>{user.fullname}</b>
                    </div>
                    <div className={styles.suggestEmail}>
                      {user.email}
                    </div>
                  </div>
                </div>
              );
            }else{
              return (
                <div className={"row suggestItem"}  onClick={()=>{this.settingMention(user, 'textarea','unlimit');}}>
                  <div className={styles.suggestProfile}><FontAwesomeIcon icon="users" style={{width:'100%',height:"100%"}} /></div>
                  <div>
                    <div style={{marginLeft:'10px'}}>
                      <b>{user.fullname}</b>
                    </div>
                  </div>
                </div>
              );
            }
          });

          let noUser = <div></div>;
          if(this.state.filterSuggest.length == 0){
            noUser = (
              <div className={"row suggestItem"}>
                <div style={{textAlign:"center"}}>
                  We didn't find any matches.
                </div>
              </div>
            );
          }
          suggestDisplay = (
            <div className={"suggestBlock"} style={{left:'10px', bottom: '90px'}}>
              <div className={"suggestHeader"}>Suggestions</div>
              {siteUser}
              {noUser}
            </div>
          );
        }else{
          suggestDisplay = <div></div>; 
        }

        let fileDispaly = <div style={{marginTop:'10px'}}><span>No file selected.</span></div>;
        if(this.state.attachmentSelect != null && this.state.attachmentSelect.length > 0){
          if(this.state.attachmentSelect.length == 1){
            fileDispaly = <div style={{marginTop:'10px'}}><span>{this.state.attachmentSelect.length} file selected.</span>: {this.state.attachmentSelect[0].name}</div>;
          }else{
            fileDispaly = <div style={{marginTop:'10px'}}><span>{this.state.attachmentSelect.length} files selected.</span></div>;
          } 
        }
        return(
          <div className={"container-fluid actionBlock"}>
            <div className="row no-gutters" style={{borderBottom:'1px solid black'}}>
              <div className="col">
                <h6><b>New Post</b></h6>
              </div>
              <div style={{cursor:'pointer'}} onClick={()=>{this.changeCurrentState('');}}><FontAwesomeIcon icon="times" /></div>
            </div>
            <div className="row no-gutters">
              <div className={"profilePicture"} style={{backgroundImage: `url("${this.state.currentUserInfo.Picture}")`, marginTop:"10px"}}></div>
              <div className="col" style={{padding:"10px"}}>
                {suggestDisplay}
                <div id="textarea" contentEditable className={"textAreaStyle"} 
                  onClick={(e)=>{this.setState({commentText: true});}}
                  onBlur={(e)=>{if(document.getElementById('textarea').innerHTML == ''){this.setState({commentText: false});}}}
                  onKeyUp={(e)=>{this.changeMentionTextArea(e,'textarea','current_team','unlimit');}}
                >
                  {span}
                </div>
                <div className="row" style={{width:'100%', textAlign:"right", marginLeft:"0px"}}>
                  <input id="c_Attachment" type="file" multiple={true} style={{display:'none'}} onChange={(e)=>{this.setState({attachmentSelect: e.target.files});}} />
                  <div className="col">{fileDispaly}</div>
                  <button className="btnWithNoBorder" type="button" style={{fontSize:'12px', marginRight:'5px', marginTop:'5px'}} onClick={()=>{ document.getElementById('c_Attachment').click(); }}><FontAwesomeIcon icon="paperclip"/></button>
                  {send_button}
                </div>
              </div>
            </div>
          </div>
        );
      }else{
        return(
          <div>
            <div className="lds-ring"><div></div><div></div><div></div><div></div></div>
            <div style={{color:"#6264a7", marginTop:"5px"}}>Wait a moment please...</div>
          </div>
        );
      }
      default: 
        return(
          <div></div>
        );
    }
  }

  private changeMentionTextArea(e, id, filterBy, selectUser){
    let textArea = document.getElementById(id).innerHTML.replace(/\&nbsp;/ig,' ');
    if(textArea.length > 0){

      //Useful
      let regx = textArea.match(/(^|\s)@([^@]*)$/i);
      if(e.keyCode == 13){
        e.preventDefault();
        if(this.state.filterSuggest.length > 0){
          let suggest = this.state.suggestSelect;
          let newHTML = '';
          if(selectUser == 'unlimit'){
            let splitByMention = textArea.split('@');
            splitByMention.forEach((mention,index)=>{
              let temp = mention.split('<div><br></div>');
              if(index > 0){
                if(temp[0] == this.state.suggestSearch){
                  newHTML += '&nbsp'+'<input class="tagPing" type="button" value="@'+this.state.filterSuggest[0].fullname+'"/>'+'&nbsp';
                }else{
                  newHTML += "@"+mention;
                }
              }else{
                if(temp[0] == this.state.suggestSearch){
                  newHTML += '&nbsp'+'<input class="tagPing" type="button" value="@'+this.state.filterSuggest[0].fullname+'"/>'+'&nbsp';
                }else{
                  newHTML += mention;
                }
              }
            });
            document.getElementById(id).innerHTML = newHTML;
            if(suggest.indexOf(this.state.filterSuggest[0]) == -1){
              suggest.push(this.state.filterSuggest[0]);
            }
          }else{
            document.getElementById(id).innerHTML = '@'+this.state.suggestSearch;
            document.getElementById(id).innerHTML = document.getElementById(id).innerHTML.replace('@'+this.state.suggestSearch,'&nbsp'+'<input class="tagPing" value="@'+this.state.filterSuggest[0].fullname+'"/>'+'&nbsp');
            if(suggest.length == 0){
              suggest.push(this.state.filterSuggest[0]);
            }else{
              suggest[0] = this.state.filterSuggest[0];
            }
          }
          this.setState({
            suggestBlockPosition: null,
            filterSuggest: [],
            suggestSearch: '',
            enablePing: false,
            suggestSelect: suggest,
            suggestBlockID: ''
          });
        }
      }else if(e.keyCode == 8){
        if(regx != null){
          let typing = this.state.suggestSearch;
          typing = regx[2];
          if(typing.length != 0){
            this.state.suggestSelect.forEach((user, index)=>{
              if(textArea.indexOf(user.fullname) < 0){
                this.state.suggestSelect.splice(index,1);
              }
            });
    
            let count = 0;
            let filter = [];
            this.siteUsers.forEach((user)=>{
              if(count < 5 && (user.fullname.toLowerCase()).indexOf((typing).toLowerCase()) > -1 && user.id != this.state.currentUserInfo.ID){
                if(filterBy == 'global'){
                  if(this.state.suggestSelect.indexOf(user) < 0){
                    count = count+1;
                    filter.push(user);
                  }
                }else{
                  if(user.siteGroup == 'Yes'){
                    if(this.state.suggestSelect.indexOf(user) < 0){
                      count = count+1;
                      filter.push(user);
                    }
                  }else{
                    if(this.state.suggestSelect.indexOf(user) < 0 && this.state.currentTab == user.team){
                      count = count+1;
                      filter.push(user);
                    }
                  }
                }
              }
            });
    
            this.setState({
              suggestSearch: typing,
              filterSuggest: filter
            });
          }
        }else{
          this.setState({
            suggestSearch: '',
            filterSuggest: [],
            enablePing: false,
            suggestBlockID: '',
            suggestBlockPosition: null
          });
        }
      }else{
        if(regx != null && !this.state.enablePing){
          let typing = this.state.suggestSearch;
          typing = regx[2];
          let filter = [];
          let count = 0;
          this.siteUsers.forEach((user)=>{
            if(count < 5 && (user.fullname.toLowerCase()).indexOf((typing).toLowerCase()) > -1 && user.id != this.state.currentUserInfo.ID){
              if(filterBy == 'global'){
                if(this.state.suggestSelect.indexOf(user) < 0){
                  count = count+1;
                  filter.push(user);
                }
              }else{
                if(user.siteGroup == 'Yes'){
                  if(this.state.suggestSelect.indexOf(user) < 0){
                    count = count+1;
                    filter.push(user);
                  }
                }else{
                  if(this.state.suggestSelect.indexOf(user) < 0 && this.state.currentTab == user.team){
                    count = count+1;
                    filter.push(user);
                  }
                }
              }
            }
          });
          this.setState({
            suggestBlockPosition: window.getSelection(),
            enablePing: true,
            filterSuggest: filter,
            suggestBlockID: id,
            suggestSearch: typing
          }); 
        }else if(regx != null && this.state.enablePing){
          let typing = this.state.suggestSearch;
          typing = regx[2];
          let count = 0;
          let filter = [];
          this.siteUsers.forEach((user)=>{
            if(count < 5 && (user.fullname.toLowerCase()).indexOf((typing).toLowerCase()) > -1 && user.id != this.state.currentUserInfo.ID){
              if(filterBy == 'global'){
                if(this.state.suggestSelect.indexOf(user) < 0){
                  count = count+1;
                  filter.push(user);
                }
              }else{
                if(user.siteGroup == 'Yes'){
                  if(this.state.suggestSelect.indexOf(user) < 0){
                    count = count+1;
                    filter.push(user);
                  }
                }else{
                  if(this.state.suggestSelect.indexOf(user) < 0 && this.state.currentTab == user.team){
                    count = count+1;
                    filter.push(user);
                  }
                }
              }
            }
          });

          this.setState({
            suggestSearch: typing,
            filterSuggest: filter
          });
        }else{
          this.setState({
            suggestBlockPosition: null,
            filterSuggest: [],
            suggestSearch: '',
            enablePing: false,
            suggestBlockID: ''
          });
        }
      }
    }else{
      this.setState({
        suggestBlockPosition: null,
        filterSuggest: [],
        suggestSearch: '',
        enablePing: false,
        suggestSelect: [],
        suggestBlockID: ''
      });
    }
  }

  private settingMention(user, id, selectUser){
    let ping = "tagPing";
    let suggest = this.state.suggestSelect;
    let textArea = document.getElementById(id).innerHTML;
    let newHTML = '';
    if(selectUser == 'unlimit'){
      let splitByMention = textArea.split('@');
      splitByMention.forEach((mention,index)=>{
        if(index > 0){
          if(mention == this.state.suggestSearch){
            newHTML += '&nbsp'+'<input class="tagPing" type="button" value="@'+user.fullname+'" />'+'&nbsp';
          }else{
            newHTML += "@"+mention;
          }
        }else{
          newHTML += mention;
        }
      });
      suggest.push(user);
      document.getElementById(id).innerHTML = newHTML;
    }else{
      document.getElementById(id).innerHTML = '@'+this.state.suggestSearch;
      document.getElementById(id).innerHTML = document.getElementById(id).innerHTML.replace('@'+this.state.suggestSearch,'&nbsp'+'<input class="tagPing" type="button" value="@'+user.fullname+'"/>'+'&nbsp');
      if(suggest.length == 0){
        suggest.push(user);
      }else{
        suggest[0] = user;
      }
    }
    this.setState({
      suggestBlockPosition: null,
      filterSuggest: [],
      suggestSearch: '',
      enablePing: false,
      suggestSelect: suggest
    });
  }

  private closeDiv(ev,data){
    //let el = ev.target as HTMLElement;
    let id = data.id ; 
    let statusUpdate = document.getElementById("btnUpdate"+id) as HTMLElement ; // el.parentElement  as HTMLElement ; 
    if(statusUpdate != null){
      if(statusUpdate.classList.contains("show")){
        statusUpdate.classList.remove("show");
        let tempStatus = [];
        cickedBtn = null;
      }
      else{
        statusUpdate.classList.add("show");
      }
    }
  }

  private resetSearch(){
    let searchBox = document.getElementById('searchBox') as HTMLInputElement;
    searchBox.value = '';
    this.setState({
      onSearching: false
    });
  }

  private settingSearchText(id){
    let searchText = document.getElementById(id) as HTMLInputElement;
    if(this.state.onSearching){
      this.setState({
        searchTxt: searchText.value
      });
    }
  }

  private settingSearchTextWhenClick(id){
    let searchText = document.getElementById(id) as HTMLInputElement;
    this.setState({
      searchTxt: searchText.value,
      onSearching: true,
      pageFile: 0,
      pageMessage: 0
    });
  }

  private settingSearch(element){
    if(!this.state.onSearching){
      if(element.keyCode == 13){
        let searchText = document.getElementById('searchBox') as HTMLInputElement;
        this.setState({
          onSearching: true,
          searchTxt: searchText.value,
          pageFile: 0,
          pageMessage: 0
        });
      }
    }
  }

  public checkLogExist = async(id): Promise<boolean> =>{
    this.webPnP = new Web(this.baseUrl);
    return new Promise<boolean>(async (res, rej)=>{
      this.webPnP.lists.getByTitle(this.pageName+id).get().then((listExists)=>{
        res(true);
      }).catch(async err =>{
        let listExist = await (await this.webPnP.lists.ensure(this.pageName+id)).list;
        await listExist.fields.addText('Log_Type', 255, { Required: false, Description: '' });
        await listExist.fields.addMultilineText('Log_Detail', 6, false, false, false, false, { Required: false, Description: '' });
        let allItemsView = await listExist.views.getByTitle('All Items');
        await allItemsView.fields.add('Log_Type');
        await allItemsView.fields.add('Log_Detail');
      });
    });
  }

  public checkListExists = async (): Promise<boolean> => {
    this.webPnP = new Web(this.baseUrl);
    return new Promise<boolean>(async (res, rej) => {
        this.webPnP.lists.getByTitle(this.pageName).get().then((listExists) => {
            res(true);
        }).catch(async err => {
            let listExists = await (await  this.webPnP.lists.ensure(this.pageName)).list;
            await listExists.fields.addMultilineText('Comments', 6, false, false, false, false, { Required: false, Description: '' });
            //await listExists.fields.addMultilineText('Likes', 6, false, false, false, false, { Required: false, Description: '' });
            await listExists.fields.addMultilineText('Description', 6, false, false, false, false, { Required: false, Description: '' });
            await listExists.fields.addDateTime('StartDate',1);
            await listExists.fields.addDateTime('EndDate',1);
            await listExists.fields.addText('FilesFolder', 255, { Required: false, Description: '' });
            let allItemsView = await listExists.views.getByTitle('All Items');
            await allItemsView.fields.add('Comments');
            //await allItemsView.fields.add('Likes');
            await allItemsView.fields.add('Description');
            await allItemsView.fields.add('StartDate');
            await allItemsView.fields.add('EndDate');
            await allItemsView.fields.add('FilesFolder');
            res(true);
        });        
    });
  }

  public getCurrentUserInfo = async () => {
    this.webPnP = new Web(this.baseUrl);
    let currentUserInfo = await this.webPnP.currentUser.get();
    this.setState({
      currentUserInfo: {
        ID: currentUserInfo.Id,
        Email: currentUserInfo.Email,
        LoginName: currentUserInfo.LoginName,
        DisplayName: currentUserInfo.Title,
        Picture: '/_layouts/15/userphoto.aspx?size=S&username=' + currentUserInfo.UserPrincipalName
      }
    });
  }

  public getPostComments = async (title, currentUserInfo) => {
    this.webPnP = new Web(this.baseUrl);
    let pagecomments = await this.webPnP.lists.getByTitle(this.pageName).items.select('Comments', 'FieldValuesAsText/Comments', 'FilesFolder', 'Id', 'Created', 'Modified')
        .filter(`Title eq '${title}'`).expand('FieldValuesAsText').get();
    if (pagecomments.length > 0) {
        var tempComments = pagecomments[0].FieldValuesAsText.Comments;
        // var tempLikes = pagecomments[0].FieldValuesAsText.Likes;
        // if (tempLikes != undefined && tempLikes != null && tempLikes !== "") tempLikes = JSON.parse(tempLikes);
        // else tempLikes = [];
        if (tempComments != undefined && tempComments != null && tempComments !== "") {
            var jsonComments = JSON.parse(tempComments);
            // if (tempLikes.length > 0) {
            //     tempLikes.map((liked) => {
            //         var fil = _.find(jsonComments, (o) => { return o.id == liked.commentID; });
            //         if (fil !== undefined && fil !== null) {
            //             fil.upvote_count = liked.userVote.length;
            //             var cufil = _.find(liked.userVote, (o) => { return o.userid == currentUserInfo.ID; });
            //             if (cufil !== undefined && cufil !== null) fil.user_has_upvoted = true;
            //         }
            //     });
            // }
            this.setState({
              allComments: jsonComments,
              fileFolder: pagecomments[0].FilesFolder,
              collabID: pagecomments[0].Id,
              createdDate: pagecomments[0].Created,
              modifiedDate: pagecomments[0].Modified
            });
            return jsonComments;
        } else {
          this.setState({
            allComments: [],
            fileFolder: "",
            collabID: -1,
            createdDate: "",
            modifiedDate: ""
          });
    
          return [];
        }
    } else {
      this.setState({
        allComments: [],
        fileFolder: "",
        collabID: -1,
        createdDate: "",
        modifiedDate: ""
      });

      return [];
    }
  }

  public updateComment = async (pageurl, comments) => {
    this.webPnP = new Web(this.baseUrl);
    let jsonComments = [];
    let pagecomments = await this.webPnP.lists.getByTitle(this.pageName).items.select('Comments', 'FieldValuesAsText/Comments', 'FilesFolder', 'Id', 'Created', 'Modified')
        .filter(`Title eq '${pageurl}'`).expand('FieldValuesAsText').get();
    if (pagecomments.length > 0) {
      var tempComments = pagecomments[0].FieldValuesAsText.Comments;
      jsonComments = JSON.parse(tempComments);
      let tempPop = jsonComments;
      let tempID = tempPop.pop().id.split('c');
      comments.id = 'c'+(parseInt(tempID[1])+1);

      let test = JSON.parse(tempComments);
      test.push(comments);
      await this.webPnP.lists.getByTitle(this.pageName).items.getById(this.state.collabID).update({
        Comments: JSON.stringify(test)
      });
      return comments.id;
    } else {
      jsonComments.push(comments);
      await this.webPnP.lists.getByTitle(this.pageName).items.add({
        Title: this.pageName,
        IsArchive: 'No',
        Comments: JSON.stringify(jsonComments)
      }).then(async (res)=>{
        this.setState({
          collabID: res.data.ID
        });
        await this.updateFilePath();
      });
      return comments.id;
    }
  }

  public updateFilePath = async () => {
    if(this.state.fileFolder == null || this.state.fileFolder == '' || this.state.fileFolder == 'null' || this.state.fileFolder == undefined || this.state.fileFolder == 'undefined'){
      await this.createFolder(`Shared Documents/Collaboration/Crisis_${this.state.collabID}_Documents`);
      await this.webPnP.lists.getByTitle(this.pageName).items.getById(this.state.collabID).update({
        FilesFolder: `Shared Documents/Collaboration/Crisis_${this.state.collabID}_Documents`
      });
    }
  }

  public getComment = async (pageurl) => {
    this.webPnP = new Web(this.baseUrl);
    let pagecomments = await this.webPnP.lists.getByTitle(this.pageName).items.select('Comments', 'FieldValuesAsText/Comments')
        .filter(`Title eq '${pageurl}'`).expand('FieldValuesAsText').get();
    if (pagecomments.length > 0) return pagecomments[0].FieldValuesAsText.Comments;
    else return null;
  }

  public createFolder = async (folderPath) => {
    this.webPnP = new Web(this.baseUrl);
    return await this.webPnP.folders.add(folderPath);
  }

  public uploadFileToFolder = async (fileinfo) => {
    let tempUpload = [];
    this.webPnP = new Web(this.baseUrl);
    for(let i = 0 ; i < fileinfo.length ; i++){
      let uploaded = await this.webPnP.getFolderByServerRelativeUrl(this.state.fileFolder).files.add(fileinfo[i].name, fileinfo[i], false);
      tempUpload.push({
        file_url: uploaded.data.ServerRelativeUrl,
        file_name: uploaded.data.Name,
        file_type: fileinfo[i].type
      });
    }
    return tempUpload;
  }

  public async createCommentJSON() {
    let textarea = document.getElementById('textarea').innerHTML;
    let contentsArr = textarea.split('&nbsp;');
    let contentStr = '';
    if(this.state.commentText){
      contentsArr.forEach((txt)=>{
        if(txt.indexOf('input class="tagPing"') > -1){
          let tempSplit = txt.split('value="');
          let tempSplit2 = tempSplit[1].split('">');
          contentStr += tempSplit2[0]+" ";
        }else{
          contentStr += txt +" ";
        }
      });
    }

    let fileUpload = [];
    if(this.state.attachmentSelect.length > 0){
      fileUpload = await this.uploadFileToFolder(this.state.attachmentSelect);
    }

    var time = new Date().toISOString();
    var commentJSON = {
      id: 'c' + (this.state.allComments.length + 1),   // Temporary id
      parent: '',
      created: time,
      modified: time,
      content: contentStr,
      pings: this.state.suggestSelect,
      fullname: this.state.currentUserInfo.DisplayName,
      profile_picture_url: this.state.currentUserInfo.Picture,
      email: this.state.currentUserInfo.Email,
      // createdByCurrentUser: true,
      // upvoteCount: 0,
      // userHasUpvoted: false,
      isPin: [],
      file: fileUpload,
      team: this.state.currentTab,
      share_to: []
    };

    let tempWait = Math.floor(Math.random() * 900) + 100;
    await this.waiting(tempWait);
    
    let id = await this.updateComment(this.pageurl, commentJSON);

    if(this.state.suggestSelect != null && this.state.suggestSelect.length > 0){
      commentJSON.id = id;
      await this.sendNotification(commentJSON, 'Collaboration',"_Log","CrisisCollaboration","Already send", commentJSON);
    }

    await this.getPostComments(this.pageurl,this.state.currentUserInfo);
    this.setState({
      parentPost: '',
      suggestBlockPosition: null,
      suggestSelect: [],
      currentState: '',
      attachmentSelect: [],
      changeScroll: true,
      alertStatus: 'Your post successfully.',
      onLoading:'',
      commentText: false
    });

    await this.waiting(1500);
    this.setState({
      alertStatus: ''
    });
  }

  public async createReplyJSON(id, status, item, item_index){
    let textarea = document.getElementById(id).innerHTML;
    let contentsArr = textarea.split('&nbsp;');
    let contentStr = '';
    if(this.state.commentText){
      contentsArr.forEach((txt)=>{
        if(txt.indexOf('input class="tagPing"') > -1){
          let tempSplit = txt.split('value="');
          let tempSplit2 = tempSplit[1].split('">');
          contentStr += tempSplit2[0]+" ";
        }else{
          contentStr += txt +" ";
        }
      });
    }

    let fileUpload = [];
    if(this.state.attachmentSelect.length > 0){
      fileUpload = await this.uploadFileToFolder(this.state.attachmentSelect);
    }

    var time = new Date().toISOString();
    var commentJSON = {
      id: 'c' + (this.state.allComments.length + 1),   // Temporary id
      parent: this.state.parentPost,
      created: time,
      modified: time,
      content: contentStr,
      pings: this.state.suggestSelect,
      fullname: this.state.currentUserInfo.DisplayName,
      profile_picture_url: this.state.currentUserInfo.Picture,
      email: this.state.currentUserInfo.Email,
      isPin: [],
      file: fileUpload,
      team: this.state.currentTab,
      share_to: []
    };
    
    if(this.state.suggestSelect != null && this.state.suggestSelect.length > 0){
      await this.sendNotification(commentJSON, 'Collaboration',"_Log", "CrisisCollaboration","Already send", commentJSON);
    }
    
    let post = await this.updateComment(this.pageurl, commentJSON);
    
    await this.getPostComments(this.pageurl,this.state.currentUserInfo);

    let tempReplyLoad = [];
    this.state.onReplyLoading.forEach((onLoad)=>{
      if(onLoad != item.id){
        tempReplyLoad.push(onLoad);
      }
    });

    this.setState({
      parentPost: '',
      toggleReply: false,
      suggestSelect: [],
      attachmentSelect: [],
      suggestBlockPosition: null,
      currentState: '',
      settingSeemore: post,
      alertStatus: 'Your reply post successfully.',
      onReplyLoading: tempReplyLoad,
      commentText: false
    });

    await this.waiting(1500);
    this.setState({
      alertStatus: ''
    });
  }

  private async sendNotification(detailJson, type, logList, pageState,status, parentComment){
    this.webPnP = new Web(this.baseUrl);
    await this.webPnP.lists.getByTitle(this.pageName+logList).items.add({
      Title: pageState,
      Log_Type: type,
      Log_Detail: JSON.stringify(detailJson),
      Status: status,
      taskid: parentComment.id
    });
  }
}